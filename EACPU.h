#pragma once
#include "Params.h"
#include "RandGenerator.h"
#include "utilsCPU.h"
#include <algorithm>

namespace eaCPU 
{
    EAParams params;

    int eaMutationTimes(int populationN)
    {
        auto ret = populationN / 100;
        return 1; 
    }

    Individual breed(const Individual& parentA, const Individual& parentB)
    {
        auto geneSize = parentA.gene.size();
        _ASSERT(geneSize == parentB.gene.size()); 

        int lower = RandGenerator::getRandomInRange(1, geneSize - 2);
        int higher = RandGenerator::getRandomInRange(lower, geneSize);

        auto ret = Individual(geneSize);

        std::copy(parentA.gene.begin() + lower, parentA.gene.begin() + higher, ret.gene.begin() + lower);

        auto ifNodeUsed = std::vector<bool>(geneSize, false);
        std::for_each(parentA.gene.begin() + lower, parentA.gene.begin() + higher, [&](int gene) {ifNodeUsed[gene] = true; });

        int indBIndex = 1;

        for (int i = 1; i < geneSize; i++)
        {
            auto& retGenI = ret.gene[i];
            if ((i > lower && i < higher) || retGenI != Individual::nulloptVal)
                continue;

            while (ifNodeUsed[parentB.gene[indBIndex]])
            {
                if (indBIndex + 1 < geneSize)
                    indBIndex++;
                else
                    break;
            }
            retGenI = parentB.gene[indBIndex];
            ifNodeUsed[retGenI] = true;
        }

        _ASSERT(ACOResults::checkAllNodesVisited(ret.gene, geneSize));

        return ret;
    }

    static std::vector<Individual> createInitPopul(int populationN, EAParams& params)
    {
        std::vector<Individual> population{};
        for (int i = 0; i < populationN; i++)
        {
            auto ind = Individual(params.nodesNum);
            ind.fillRandom();
            ind.countFitness(params.edgesDist.data());
            population.push_back(ind);
        }
        return population;
    }

    inline int getElementRoulette(const std::vector<double>& percentages, const auto random)
    {
        const auto& it = std::lower_bound(percentages.begin(), percentages.end(), random);
        if (it != percentages.end()) [[likely]]
            return std::distance(percentages.begin(), it);
        else
            return percentages.size() - 1;
    }

    std::vector<std::pair<int, int>> getMatingPool(const std::vector<Individual>& population)
    {
        decltype(Individual::distance) distSum{};
        for (const auto indv : population)
        {
            _ASSERT(indv.distance > 0);
            distSum += indv.distance;
        }

        std::vector<double> percentages{};
        percentages.reserve(population.size());

        double percentageSum{};
        for (const auto indv : population)
        {
            percentageSum += (double)indv.distance / (double)distSum;
            percentages.push_back(percentageSum);
        }

#ifdef _DEBUG
        if (!(percentageSum > 0.999 && percentageSum < 1.001))
            assert(false);
#endif // DEBUG
        std::vector<std::pair<int, int>> ret{};
        for (int i = 0; i < population.size(); i++)
        {
            auto random1 = RandGenerator::getRandomInRange(0.0, 1.);
            auto random2 = RandGenerator::getRandomInRange(0.0, 1.);

            ret.push_back({ getElementRoulette(percentages, random1), getElementRoulette(percentages, random2) });
        }

        return ret;
    }

    void sortElite(std::vector<Individual>& population, const size_t eliteSize)
    {
        std::partial_sort(population.begin(), population.begin() + eliteSize, population.end(), [&](const auto& el1, const auto& el2) {return el1.distance < el2.distance; });
    }

    void sortWeakest(std::vector<Individual>& population, const size_t eliteSize)
    {
        std::partial_sort(population.begin(), population.begin() + eliteSize, population.end(), [&](const auto& el1, const auto& el2) {return el1.distance > el2.distance; });
    }

    static std::vector<Individual> getOffspring(const std::vector<std::pair<int, int>> matingPool, const std::vector<Individual>& population, const auto mutationRate, const auto mutationTimes, EAParams& params)
    {
        std::vector<Individual> ret{};
        for (const auto& [gene1, gene2] : matingPool)
        {
            auto kid = breed(population[gene1], population[gene2]);

            auto rand = RandGenerator::getRandomInRange(0., 1.);
            if (rand < mutationRate)
            {
                for (int i = 0; i < mutationTimes; i++) {
                    //kid.mutate();
                    kid.reverseSA();
                }
            }
            kid.countFitness(params.edgesDist.data());
            ret.push_back(kid);
        }
        return ret;
    }

    void startEACPU(const EAParams& _params, EAResults& results)
    {
        std::cout << "EA CPU ST start\n";

        params = _params;
        params.checkParams();

        results.timer.timerName = "EA CPU ST";
        results.timer.start("total");

        const size_t eliteSize = static_cast<size_t>(static_cast<float>(params.populationN) * params.elitePercentage);

        auto population = createInitPopul(params.populationN, params);
        auto mutationTimes = eaMutationTimes(params.populationN);

        float minFoundDistance = std::numeric_limits<float>::max();
        std::string currentIterationName;
        for (int i = 0; i < params.generations; i++)
        {
            constexpr bool logGeneration = true;// (i % 100 == 0);
            std::string currentIterationName;
            if constexpr (logGeneration) {
               currentIterationName = "iteration" + std::to_string(i);
                results.timer.start(currentIterationName);
            }

            auto matingPool = getMatingPool(population);
            auto offspring = getOffspring(matingPool, population, params.mutationRate, mutationTimes, params);

            sortElite(population, eliteSize);
            sortWeakest(offspring, eliteSize);

            _ASSERT(population.size() == offspring.size());

            for (int j = 0; j < eliteSize; j++)
            {
                offspring[j] = population[j];
            }

            population = offspring;

            if constexpr (logGeneration) 
            {
                results.timer.stop(currentIterationName);
                auto lowestElement = *std::min_element(population.begin(), population.end(), [](const auto& el1, const auto& el2) {return el1.distance < el2.distance; });
                
                results.iterations.emplace_back(Results::Iteration(lowestElement.gene, lowestElement.distance));
                if (i % 20 == 0) {
                    printIteration("EA CPU ST", results.timer.millisSinceBeginning("total"), lowestElement.distance, i, params.generations);
                }
            }
        }

        results.timer.stop("total");
    }
}

namespace EACPUMulti 
{
    EAParams params;

    std::vector<std::vector<Individual>> populations;
    
    std::atomic<int> threadMutationTimes{};
    std::atomic<uint64_t> totalIterations{};

    struct ThreadData 
    {
        int threadId;
        std::atomic_bool processing = true;
        static inline std::atomic_bool runThreads = true; 
        static inline int iterWoSync{};
        static inline int eliteSize{};
        int n{};
    };
    std::vector<std::unique_ptr<ThreadData>> threadData;

    void sortByBest(std::vector<Individual>& population)
    {
        std::sort(population.begin(), population.end(), [&](const auto& el1, const auto& el2) {return el1.distance < el2.distance; });
    }

    void mergeBestIntoVec(std::vector<Individual>& dst, const std::vector<Individual>& src) 
    {
        _ASSERT(dst.size() == src.size());

        auto tmp = std::vector<Individual>();

        int dstIdx = 0;
        int srcIdx = 0;
        
        while (tmp.size() < dst.size()) 
        {
            if(dst[dstIdx].distance < src[srcIdx].distance && dstIdx < dst.size()) 
            {
                tmp.push_back(dst[dstIdx]);
                dstIdx++;
            }
            else if(srcIdx < dst.size())
            {
                tmp.push_back(src[srcIdx]);
                srcIdx++;
            }
        }
        dst = tmp;
    }
    
    static inline std::atomic<uint64_t> debugiterCounter{};

    void loop(ThreadData& data)
    {
        auto& tPopulation = populations[data.threadId];

        while (true)
        {
            if (data.processing)
            {
                data.n = 0;

                while (data.n < data.iterWoSync && data.runThreads)
                {
                    data.n++;
                    debugiterCounter++;
                    auto matingPool = eaCPU::getMatingPool(tPopulation);
                    auto offspring = eaCPU::getOffspring(matingPool, tPopulation, params.mutationRate, static_cast<int>(threadMutationTimes), params);

                    eaCPU::sortElite(tPopulation, data.eliteSize);
                    eaCPU::sortWeakest(offspring, data.eliteSize);

                    _ASSERT(tPopulation.size() == offspring.size());

                    for (int j = 0; j < data.eliteSize; j++)
                    {
                        offspring[j] = tPopulation[j];
                    }

                    tPopulation = offspring;
                }
                data.processing = false;
            }
            if (!data.runThreads)
            {
                return;
            }
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(1ms);
        }
    }

    void startEACPUMulti(const EAParams& _params, EAResults& results)
    {
        params = _params;
        params.checkParams();
        const int numThreads = _params.threads;

        results.timer.timerName = "EA CPU MT";
        results.timer.start("total");

        ThreadData::runThreads = true;
        ThreadData::iterWoSync = 0;
        ThreadData::eliteSize=0;
        totalIterations={};
        debugiterCounter = {}; //DEBUGDEBUG DEBUG

        assert(params.populationN >= numThreads);
        assert(params.populationN % numThreads == 0);

        const int threadPopulationN = params.populationN / numThreads;
        const size_t tEliteSize = static_cast<size_t>(static_cast<float>(threadPopulationN) * params.elitePercentage);

        assert(threadPopulationN > 0);

        populations = std::vector<std::vector<Individual>>();
        threadData = std::vector<std::unique_ptr<ThreadData>>();

        ThreadData::iterWoSync = params.iterWoSync; 
        ThreadData::eliteSize = tEliteSize;

        threadData.clear();
        for (int n = 0; n < numThreads; n++)
        {
            threadData.emplace_back(new ThreadData());
            threadData[n].get()->threadId = n;
            populations.emplace_back(eaCPU::createInitPopul(threadPopulationN, params));
        }
        threadMutationTimes = eaCPU::eaMutationTimes(threadPopulationN);

        auto globalElite = std::vector<Individual>(populations[0]);

        std::atomic<float> minFoundDistanceGlobal = std::numeric_limits<float>::max();
        std::vector<std::thread> threads(numThreads);

        for (int n = 0; n < numThreads; n++)
        {
            threads[n] = std::thread(EACPUMulti::loop, std::ref(*threadData[n].get()));
        }

        while (totalIterations < params.generations)
        {
            for (auto& tDataPtr : threadData)
            {
                auto& tData = *tDataPtr; 
                if (!tData.processing)
                {
                    auto& tPopulation = populations[tData.threadId];

                    totalIterations += tData.n;
                    tData.n = {};

                    sortByBest(tPopulation);
                    sortByBest(globalElite);

                    mergeBestIntoVec(globalElite, tPopulation);

                    //tPopulation = globalElite;

                    for (int j = 0; j < tEliteSize; j++)
                    {
                        tPopulation[threadPopulationN-1-j]= globalElite[j];
                    }


                    printIteration("EA CPU MT", results.timer.millisSinceBeginning("total"), globalElite[0].distance, totalIterations, params.generations);
                    results.iterations.emplace_back(Results::Iteration(globalElite[0].gene, globalElite[0].distance));
                }
                if (totalIterations < params.generations) {
                    tData.processing = true;
                }
                else {
                    break;
                }
            }
        }

        ThreadData::runThreads = false;
        std::cout << "END totalIt:" << totalIterations << " dist:" << globalElite[0].distance << std::endl;
        std::cout << "debug it counter: " << debugiterCounter << std::endl;

        for (auto& thread : threads)
        {
            thread.join();
        }

        results.timer.stop("total");

        results.iterations.emplace_back(Results::Iteration(globalElite[0].gene, globalElite[0].distance));
        
    }
}