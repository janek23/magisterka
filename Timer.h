#pragma once
#include <chrono>
#include <map>
#include <unordered_map>
#include <string>
#include <iomanip> 
#include <sstream>
#include <utility>
#include <numeric>

class Timer 
{
public:
	using TIMEPOINT = std::chrono::time_point<std::chrono::high_resolution_clock>;

	struct Measurement
	{
		TIMEPOINT begin;
		TIMEPOINT end;

		double getMicros() const
		{
			return std::chrono::duration<double, std::micro>(end - begin).count();
		}

		double getMillis() const
		{
			return std::chrono::duration<double, std::milli>(end - begin).count();
		}

		std::string toString() const
		{
			std::stringstream ss;
			ss << getMillis() << " [ms]";
			return ss.str();
		}

	};
private:
	std::map<std::string, Measurement> measurements{};
public:
	Timer() = default;
	Timer(std::string timerName) :timerName(timerName) {}

	std::string timerName{};
	void start(std::string name = std::string()) 
	{
		_ASSERT(!contains(name));
		Measurement measurement{};
		measurement.begin = getNow();
		measurements.emplace(std::make_pair(name, measurement));
	}

	void stop(std::string name = std::string()) 
	{
		_ASSERT(contains(name));
		measurements.at(name).end = getNow();
	}

	double millisSinceBeginning(std::string name = std::string())
	{
		const auto& begin = measurements.at(name).begin;
		const auto now = getNow();
		return std::chrono::duration<double, std::milli>(now - begin).count();
	}

	Measurement get(std::string name = std::string())
	{
		_ASSERT(contains(name));
		return measurements.at(name);
	}

	static double average(const std::vector<double>& vec)  
	{
		return std::accumulate(vec.begin(), vec.end(), 0.0) / vec.size();
	}

	static double median(std::vector<double> vec)
	{
		if (vec.empty()) {
			return -1;
		}

		std::sort(vec.begin(), vec.end());
		return vec[vec.size() / 2];
	}

	static double max(const std::vector<double>& vec)
	{
		if (vec.empty())
			return -1;

		return *std::max_element(vec.begin(), vec.end());
	}

	static double min(const std::vector<double>& vec)
	{
		if (vec.empty())
			return -1;

		return *std::min_element(vec.begin(), vec.end());
	}

	std::vector<double> measurementsVecMillis(const std::map<std::string, Measurement>& map) const
	{
		std::vector<double> tmp{};
		for (const auto& nameMeasure : map)
		{
			if (nameMeasure.first == "total") {
				continue;
			}

			tmp.push_back(nameMeasure.second.getMillis());
		}
		return tmp;
	}

	std::string toString() const
	{
		std::stringstream ss;
		ss << std::setprecision(3);
		ss << std::fixed;
		ss << "Timer:" << timerName<< std::endl;

		auto orderedMap = std::map<std::string, Measurement>(measurements.begin(), measurements.end());
		auto vecMillis = measurementsVecMillis(orderedMap);

		ss << "average" << std::setw(10) << average(vecMillis) << " [ms]\n";
		ss << "median" << std::setw(10) << median(vecMillis) << " [ms]\n";
		ss << "biggest" << std::setw(10) << max(vecMillis) << " [ms]\n";
		ss << "lowest" << std::setw(10) << min(vecMillis) << " [ms]\n";
		ss << "total" << std::setw(10) << orderedMap.at("total").getMillis() << " [ms]\n";
		ss << "END timer:" << timerName << std::endl;

		for (const auto& nameMeasure : orderedMap)
		{
			ss << nameMeasure.first << std::setw(10) << nameMeasure.second.getMillis() << " [ms]\n";
		}

		return ss.str();
	}

private:
	bool contains(const std::string key) {return measurements.find(key) != measurements.end();}

	TIMEPOINT getNow()
	{
		return std::chrono::high_resolution_clock::now();
	}

};