#pragma once

#include <filesystem>
#include <fstream>

#include "Graph.h"
#include "nlohmann/json.hpp"

class FileManager
{
public:
	static nlohmann::json serializeGraph(const Graph& graph)
	{
		nlohmann::json j;
		for (const auto& nodeShdPtr : graph.nodes) {
			nlohmann::json node;
			to_json(node, *nodeShdPtr.get());
			j.push_back(node);
		}

		return j;
	}

	static Graph deserializeGraph(const nlohmann::json& graph)
	{
		std::vector<Graph::Node> nodes{};
		int iterations = 0;
		for (const auto& j : graph)
		{
			nodes.push_back(Graph::Node(j["x"], j["y"], j["id"]));
		}

		return Graph::generateGraph(nodes);
	}

	static Graph deserializeGraph(std::string graph)
	{
		auto json = nlohmann::json::parse(graph);
		return deserializeGraph(json);
	}


	static bool saveFile(const std::string& fileContent, const std::string fileName, const std::string dir)
	{
		namespace fs = std::filesystem;

		if (fs::exists("/" + dir + "/" + fileName)) {
			return false;
		}

		if (not fs::exists(dir))
		{
			fs::create_directory(dir);
		}
		try {
			std::ofstream fstream(dir+"/"+fileName + ".json", std::ofstream::out);
			fstream << fileContent;
			fstream.close();
		}
		catch (std::exception& ex) {
			std::cout << ex.what() << std::endl;
		}
		return true;
	}

	static std::string loadFromFile(const std::string& fileName, const std::string dir) 
	{
		namespace fs = std::filesystem;

		if (not fs::exists(dir + "/" + fileName)) {
			throw; //TODO FOR DEBUG ONLY 
		}

		try {
			std::ifstream fstream(dir + "/" + fileName);
			if (fstream.is_open()) {
				return std::string((std::istreambuf_iterator<char>(fstream)), std::istreambuf_iterator<char>());
			}
		}
		catch (std::exception& ex) {
			std::cout << ex.what() << std::endl;
		}
		return "";
	}

	static std::vector<std::string> getFileNames(const std::string dir) 
	{
		namespace fs = std::filesystem;
		std::vector <std::string> retVec;
		for (const auto& file : fs::directory_iterator(dir)) 
		{
			retVec.push_back(file.path().filename().string());
		}
		return retVec;
	}

private:
	static void to_json(nlohmann::json& j, const Graph::Node& node)
	{
		j["id"] = node.id;
		j["x"] = node.x;
		j["y"] = node.y;
	}

};
