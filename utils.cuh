#pragma once

#include <curand.h>
#include <curand_kernel.h>
#include <type_traits>
#include <iomanip>
#include <map>
#include "stdio.h"
#include <numeric>
#include <random>
#include <thrust/reduce.h>
#include <thrust/execution_policy.h>
#include <cassert>


namespace cudaUtils 
{
	constexpr size_t MAX_NODES = 5000;//hack set also in view

	static void printIterationCUDA(const std::string& algoName, double totalTime, float bestResult, int currentIt, int maxIt)
	{
		currentIt++;
		std::cout << std::fixed << std::setprecision(3) << algoName << " time: " << totalTime << " [ms] result: " << bestResult << " " << currentIt << "/" << maxIt << " " << currentIt * 100 / maxIt << "[%]" << std::endl;
	}

	template<typename T>
	__device__  inline T& value(T* data, const int node1, const int node2, const int nodesN)
	{
		return *(data + (node1 * nodesN + node2));
	}

	__global__ static void initRandomStates(curandState_t* states, unsigned int seed)
	{
		int id = threadIdx.x + blockIdx.x * blockDim.x;
		curand_init(seed, id, 0, &states[id]);
	}

	__device__ static float getRandomInRange_0f_1f(curandState& localState)
	{
		return curand_uniform(&localState);
	}

	//https://stackoverflow.com/questions/24537112/uniformly-distributed-pseudorandom-integers-inside-cuda-kernel/24537113#24537113
	__device__ static int getRandomInRange_FAST(int max, int min, curandState& localState)
	{
		float randFloat = curand_uniform(&localState);
		randFloat *= (max - min + 0.999999);
		randFloat += min;
		return __float2int_rz(randFloat);
	}

	__device__ static int getRandomInRange_SLOW(int max, int min, curandState& localState)
	{
		return min + curand(&localState) % (max - min);
	}

	static void checkCudaDevices()
	{
		int deviceCount = 0;
		cudaGetDeviceCount(&deviceCount);
		if (deviceCount == 0)
		{
			printf("ERROR NO COUDA DEVICE FOUND \n");
			throw;
		}
		else
		{
			printf("Found %d CUDA Devices\n", deviceCount);
			cudaDeviceProp prop;
			for (int i = 0; i < deviceCount; i++) {
				cudaGetDeviceProperties(&prop, i);
				printf("Device Number: %d\n", i);
				printf("  Device name: %s\n", prop.name);
				printf("  Memory Clock Rate (KHz): %d\n", prop.memoryClockRate);
				printf("  Memory Bus Width (bits): %d\n", prop.memoryBusWidth);
				printf("  Peak Memory Bandwidth (GB/s): %f\n", 2.0 * prop.memoryClockRate * (prop.memoryBusWidth / 8) / 1.0e6);
				printf("  Global memory available on device (MB): %f\n", prop.totalGlobalMem / 1.0e6);
				printf("  Shared memory available per block (B): %d\n", (int)prop.sharedMemPerBlock);
				printf("  Maximum number of threads per block: %d\n", prop.maxThreadsPerBlock);
				printf("  Maximum number of threads per MultiProcessor: %d\n", prop.maxThreadsPerMultiProcessor);
				printf(" \n");
			}
		}
	}

	inline void checkCudaError(cudaError_t err)
	{
#ifdef _DEBUG
		if (err != cudaSuccess)
		{
			printf("CUDA error: %s\n", cudaGetErrorString(err));
			throw;
		}
#endif // DEBUG
	}

	inline void checkCudaError()
	{
#ifdef _DEBUG
		{
			cudaDeviceSynchronize();
			checkCudaError(cudaGetLastError());
		}
#endif // DEBUG
	}

	template<typename T>
	class RAII_MALLOC
	{
		static_assert(std::is_pointer<T>::value, "");

		RAII_MALLOC(RAII_MALLOC&& other) = default;
		RAII_MALLOC(const RAII_MALLOC& other) = delete;
		RAII_MALLOC& operator=(const RAII_MALLOC& other) = delete;
		RAII_MALLOC& operator=(const RAII_MALLOC&& other) = delete;

	public:
		T devPtr;

		RAII_MALLOC(T& _devPtr, const size_t size)
		{
			checkCudaError(
				cudaMalloc((void**)&_devPtr, size)
			);
			this->devPtr = _devPtr;
		}

		~RAII_MALLOC()
		{
			checkCudaError(cudaFree(devPtr));
		}
	};

	inline std::pair<int, int> threadBlocksLUT(const int iterationsN)
	{
		if (iterationsN <= 64)
			return { 1, 64 };
		else if (iterationsN <= 128)
			return { 2, 64 };
		else if (iterationsN <= 256)
			return { 4, 64 };
		else if (iterationsN <= 512)
			return { 4, 128 };
		else if (iterationsN <= 768)
			return { 6, 128 };
		else if (iterationsN <= 1024)
			return { 8, 128 };
		else if (iterationsN <= 2048)
			return { 16, 128 };
		else if (iterationsN <= 4096)
			return { 32, 128 };
		else if (iterationsN <= 6144)
			return { 48, 128 };
		else if (iterationsN <= 8192)
			return { 64, 128 };


		assert(false);
		return { 64, 1024 };
	}

	class Individual
	{
	public:
		Individual() = default;

		int geneArr[MAX_NODES];
		float distance = -1.f;

		__device__ void countFitness(const int nodesN, float const * d_distances)
		{
			distance = 0;
			for (int i = 0; i + 1 < nodesN; i++)
			{
				distance += value(d_distances, geneArr[i], geneArr[i + 1], nodesN);
			}
			distance += value(d_distances, geneArr[nodesN - 1], geneArr[0], nodesN);
		}

		__device__ void countFitnessSAReverse(const int nodesN, float const * d_distances, const thrust::pair<int, int>& reverseParams)
		{
			const auto& lower= reverseParams.first;
			const auto& higher = reverseParams.second;

			if (lower == higher)
				return;

			auto A = lower - 1;
			auto B = higher - 1;

			auto& AB = value(d_distances, geneArr[A], geneArr[B], nodesN);
			auto& higherLower = value(d_distances, geneArr[higher], geneArr[lower], nodesN);
			auto& Alower = value(d_distances, geneArr[A], geneArr[lower], nodesN);
			auto& Bhigher = value(d_distances, geneArr[B], geneArr[higher], nodesN);

			distance = distance - AB - higherLower + Alower + Bhigher;
		}

		__device__ bool operator <(const Individual& ind) const
		{
			return distance < ind.distance;
		}
	};

	struct IndvCMP
	{
		__host__ __device__
			bool operator()(const Individual& i1, const Individual& i2)
		{
			return i1.distance < i2.distance;
		}
	};

	struct IndvCMP_Inverse
	{
		__host__ __device__
			bool operator()(const Individual& i1, const Individual& i2)
		{
			return i1.distance > i2.distance;
		}
	};

	__host__ static std::vector<int> getRandomNodesVec(int nodesN)
	{
		_ASSERT(nodesN > 3);
		std::vector<int> tmpVec(nodesN);
		tmpVec[0] = 0;
		std::iota(tmpVec.begin() + 1, tmpVec.end(), 1);
		static std::random_device rd;
		static std::mt19937 mt(rd());
		std::shuffle(tmpVec.begin() + 1, tmpVec.end(), mt);
		return tmpVec;
	}

	__host__ static std::vector<Individual>  createInitPopulation(const int populationN, const int nodesN)
	{
		std::vector<Individual> population(populationN);
		for (int i = 0; i < populationN; i++)
		{
			auto ind = Individual();
			auto randomNodesVec = getRandomNodesVec(nodesN);
			std::copy(randomNodesVec.begin(), randomNodesVec.end(), ind.geneArr);
			population[i] = ind;
		}
		return population;
	}

	__device__ static thrust::pair<int, int> drawLowHighNodesIdx(const int nodesN, curandState& localState)
	{
		int lower = getRandomInRange_FAST(0, nodesN - 2, localState);
		int higher = getRandomInRange_FAST(lower, nodesN - 1, localState);

#ifdef _DEBUG
		assert(lower > 0 && lower < nodesN - 2);
		assert(lower < higher&& higher < nodesN - 1);
#endif // _DEBUG

		return { lower, higher };
	}

	__device__ static void mutateSwap(Individual& indv, const int nodesN, curandState& localState)
	{
		const auto& indexes = drawLowHighNodesIdx(nodesN, localState);
		thrust::swap(indv.geneArr[indexes.first], indv.geneArr[indexes.second]);
	}

	__device__ static void mutateReverse(Individual& indv, const int nodesN, const thrust::pair<int, int>& param) 
	{
		thrust::reverse(thrust::device, indv.geneArr + param.first, indv.geneArr + param.second);
	}

	__device__ static thrust::pair<int, int> mutateReverse(Individual& indv, const int nodesN, curandState& localState)
	{
		const auto& indexes = drawLowHighNodesIdx(nodesN, localState);
		mutateReverse(indv, nodesN, indexes);
		return indexes;
	}

}
