#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <limits>
#include <algorithm>
#include <thrust/device_vector.h>
#include <thrust/extrema.h>
#include <thrust/execution_policy.h>

#include "Params.h"
#include "Timer.h"
#include "utils.cuh"
#include <cuda_runtime.h>

namespace saKernel 
{
	using namespace cudaUtils;
	using Temp = SAParams::Temp;
	
	SAParams params;
	CUDAParams cudaParams;

	curandState_t* states{};
	float* d_distances{};

	double* d_temp{};
	float* d_bestDistances{};
	int* d_acceptedWorsed{};
	int* d_itWoImproval{};
	
	Individual* d_population; 

	__device__ __host__ void updateTempLinear(Temp& temp, double multipler) 
	{
		temp *= multipler;
	}

	__device__ double acceptanceFunct(float delta, Temp temp)
	{
		return exp(-delta / temp);
	}

	__device__ bool isSolutionAccepted(float distBefore, float distAfter, Temp temp, curandState& localState)
	{
		if (distAfter < distBefore)
			return true;

		auto random = getRandomInRange_0f_1f(localState);
		if (acceptanceFunct(distAfter - distBefore, temp) > random)
		{
			//acceptedWorsed++;
			return true;
		}

		return false;
	}
	__device__ int bestId;
	__global__ void processGPU(const int populationSize, const int nodesN, const int maxIterations, const double Tmin, double* temperatureArr,
		int* itWoImprovalArr, float* distances, Individual* population, float* bestDistances, curandState_t* randState)
	{
		const int i = blockIdx.x * blockDim.x + threadIdx.x;

		__shared__ unsigned int n;

		if (i < populationSize)
		{
			//auto& const itWoImproval = itWoImprovalArr[i];
			auto& const temp = temperatureArr[i];
			auto& const indv = population[i];
			auto& const bestDistance = bestDistances[i];
			thrust::pair<int, int> mutateParams;

			indv.countFitness(nodesN, distances);
			float distBefore{};
			float distAfter{};
			while (temp > Tmin && n < maxIterations)
			{
				atomicAdd(&n, 1);
				distBefore = indv.distance;
				mutateParams = mutateReverse(indv, nodesN, randState[i]);
				indv.countFitnessSAReverse(nodesN, distances, mutateParams);
				distAfter = indv.distance;

				if (isSolutionAccepted(distBefore, distAfter, temp, randState[i]))
				{
					bestDistance = distAfter;
					//itWoImproval = {};
					updateTempLinear(temp, 1 - 0.000001);
				}
				else
				{
					mutateReverse(indv, nodesN, mutateParams);
					indv.distance = distBefore;
					//itWoImproval++;
				}
			}
		}

		__syncthreads();
		
		if (i ==0)
		{
			float bestDist = FLT_MAX;
			for (int j = 0; j < populationSize; j++)
			{
				const auto& indv = population[j];
				if (indv.distance < bestDist)
				{
					bestDist = indv.distance;
					bestId = j;
				}
			}
		}
	}

	__global__ void copyBestIndvToThreads(const int populationSize, Individual* population) 
	{
		const int i = blockIdx.x * blockDim.x + threadIdx.x;

		if (i < populationSize && i != bestId)
		{
			population[i].distance = population[bestId].distance;
			memcpy(population[i].geneArr, population[bestId].geneArr, sizeof(int) * MAX_NODES);
		}
	}

	void cudaSaAlgorithm(const SAParams& _params, SAResults& results)
	{
		std::cout << "SA GPU start\n";

		params = _params;
		params.checkParams();

		results.timer.timerName = "SA GPU";
		results.timer.start("total");

		const auto cudaParams = threadBlocksLUT(params.CUDAThreads);

		RAII_MALLOC<decltype(states)> randStatesRAII(states, cudaParams.first * cudaParams.second * sizeof(curandState_t));
		initRandomStates << <cudaParams.first, cudaParams.second >> > (states, time(0));
		checkCudaError();

		RAII_MALLOC<decltype(d_distances)> distancesRAII(d_distances, params.edgesDist.size() * sizeof(float));
		RAII_MALLOC<decltype(d_population)> populationRAII(d_population, params.CUDAThreads * sizeof(Individual));

		cudaMemcpy(d_distances, params.edgesDist.data(), params.edgesDist.size() * sizeof(float), cudaMemcpyHostToDevice);
		checkCudaError();

		auto initPopulation = createInitPopulation(params.CUDAThreads, params.nodesNum);

		cudaMemcpy(d_population, initPopulation.data(), initPopulation.size() * sizeof(Individual), cudaMemcpyHostToDevice);
		checkCudaError();

		RAII_MALLOC<decltype(d_bestDistances)> bestDistanceRAII(d_bestDistances, params.CUDAThreads * sizeof(decltype(d_bestDistances)));
		std::vector<float> bestDistancesVec(params.CUDAThreads, std::numeric_limits<float>::max());
		cudaMemcpy(d_bestDistances, bestDistancesVec.data(), bestDistancesVec.size() * sizeof(float), cudaMemcpyHostToDevice);

		RAII_MALLOC<decltype(d_temp)> tempRAII(d_temp, params.CUDAThreads * sizeof(decltype(d_temp)));
		std::vector<double> tempVec(params.CUDAThreads, params.Tmax);
		cudaMemcpy(d_temp, tempVec.data(), tempVec.size() * sizeof(double), cudaMemcpyHostToDevice);

		d_itWoImproval = nullptr;
		uint64_t i{};
		int printMessageEveryIts = params.iterWoSync * 10;
		while (i < params.maxIterations) 
		{
			const std::string currentIterationName = "iteration" + std::to_string(i);
			results.timer.start(currentIterationName);

			processGPU << < cudaParams.first, cudaParams.second >> > (params.CUDAThreads, params.nodesNum, params.iterWoSync, params.Tmin, d_temp,
				 d_itWoImproval, d_distances, d_population, d_bestDistances, states);
			checkCudaError();

			copyBestIndvToThreads << <cudaParams.first, cudaParams.second >> > (params.CUDAThreads, d_population);
			checkCudaError();

			cudaDeviceSynchronize();

			i += params.iterWoSync;
			
			results.timer.stop(currentIterationName);
			if (i == printMessageEveryIts) {
				printMessageEveryIts += params.maxIterations / 100;

				cudaMemcpy(initPopulation.data(), d_population, initPopulation.size() * sizeof(Individual), cudaMemcpyDeviceToHost);
				auto minEl = *std::min_element(initPopulation.begin(), initPopulation.end(), [](auto& indv1, auto& indv2) {return indv1.distance < indv2.distance; });

				printIterationCUDA("SA CUDA", results.timer.millisSinceBeginning("total"), minEl.distance, i, params.maxIterations);
				//std::cout << " SA CUDA totalIterations: " << i << "/" << params.maxIterations << "  " << (float)i * 100.f / (float)params.maxIterations << "% itTime: " << results.timer.get(currentIterationName).getMillis() << " [ms]\n";
			}
#ifdef _DEBUG
			cudaMemcpy(initPopulation.data(), d_population, initPopulation.size() * sizeof(Individual), cudaMemcpyDeviceToHost);
			auto maxEl = *std::max_element(initPopulation.begin(), initPopulation.end(), [](auto& indv1, auto& indv2) {return indv1.distance < indv2.distance; });
			std::cout << "bestIndv:"<< maxEl.distance<<"\n";
#endif // _DEBUG

		}
		cudaMemcpy(initPopulation.data(), d_population, initPopulation.size() * sizeof(Individual), cudaMemcpyDeviceToHost);
		auto& bestIndv = *std::min_element(initPopulation.begin(), initPopulation.end(), [](const auto& a, const auto& b) {return a.distance < b.distance; });

		Results::Iteration iter{};
		iter.bestPath = std::vector<int>(bestIndv.geneArr, bestIndv.geneArr + params.nodesNum);
		iter.bestPathLength = bestIndv.distance;
		results.iterations.emplace_back(iter);

		results.timer.stop("total");
	}
}



