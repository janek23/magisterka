#pragma once
#include <cstdint>
#include <memory>
#include <vector>
#include <stdexcept>
#include <cmath>

#include "RandGenerator.h"

class Graph 
{
public:

	class Edge;

	class Node {
	public: 
		Node(float x, float y, uint16_t id): x(x), y(y), id(id) {}
		Node(const Node& node) :x(node.x), y(node.y), id(node.id) {}

		const int id;
		const float x;
		const float y;

		std::vector<Edge*>edges{};
	};

	class Edge 
	{
	public:
		Edge(Node* a, Node* b) : a(a), b(b), dist(getDist(a, b)){}

		Node* const a;
		Node* const b;
		const float dist{};

		Node* const secondNode(const Node* node) const 
		{
			if (node == a) {
				return b;
			}
			else {
				return a;
			}
		}

	private:
		static inline float getDist(const Node* a, const Node* b)
		{
			if (!(a and b))
				throw std::runtime_error("Node is nullptr");

			float distA = a->x - b->x;
			float distB = a->y - b->y;
			return std::sqrtf((distA * distA) + (distB * distB));
		}

	};

	std::vector<std::shared_ptr<Node>> nodes{};
	std::vector<std::shared_ptr<Edge>> edges{};

	static Graph generateRandomGraph(float xMin, float xMax, float yMin, float yMax, uint16_t numElements) 
	{
		using namespace RandGenerator;
		std::vector<Node> nodes{};
		for (int i = 0; i < numElements; i++) {
			nodes.emplace_back(Node(getRandomInRange(xMin, xMax), getRandomInRange(yMin, yMax), i));
		}
		return generateGraph(nodes);
	}

	static Graph generateGraph(const std::vector<Node>& nodes) 
	{
		Graph graph{};

		for (const auto& node : nodes)
		{
			auto nodePtr = std::make_shared<Node>(node.x, node.y, node.id);
			graph.nodes.push_back(std::move(nodePtr));
		}

		int i = 0;
		for (const auto& node : graph.nodes) {
			auto tmpI = i;
			for (; tmpI < graph.nodes.size(); tmpI++)
			{
				auto nodeA = node.get();
				auto nodeB = graph.nodes[tmpI].get();
				if (nodeA != nodeB) {
					auto edge = std::make_shared<Edge>(nodeA, nodeB);
					nodeA->edges.push_back(edge.get());
					nodeB->edges.push_back(edge.get());
					graph.edges.push_back(std::move(edge));
				}
			}
			i++;
		}

#ifdef _DEBUG
		auto n = graph.nodes.size();
		if (graph.edges.size() != (n * (n - 1)) / 2)
			throw;
#endif // DEBUG

		return graph;
	}


	std::map < std::pair<int, int>, std::shared_ptr<Edge>> createHelperEdgesMap() const 
	{
		std::map < std::pair<int, int>, std::shared_ptr<Edge>> map;
		std::for_each(edges.begin(), edges.end(), [&](auto& edge) {
			map.insert({ { edge->a->id, edge->b->id }, edge });
			map.insert({ { edge->b->id, edge->a->id }, edge });
			});
		return map;
	}
	std::vector<std::shared_ptr<Edge>> getPathEdgesVec(const std::vector<int>& edges) const
	{
		std::vector<std::shared_ptr<Edge>> ret{};

		auto helperMap = createHelperEdgesMap();

		for (int i = 0; i+1 < edges.size(); i++) 
		{
			auto& edge = helperMap.at({ edges[i], edges[i + 1] });
			ret.push_back(edge);
		}
		ret.push_back(helperMap.at({ *(edges.end() - 1), edges[0] }));
	
		return ret;
	}

	std::vector<float> flattenedGraph() const 
	{
		std::vector<float>ret{};
		for (const auto& node : nodes) 
		{
			for (const auto& edge : node.get()->edges)
			{
				ret.push_back(edge->dist);
			}
		}
		return ret;
	}

	std::vector<float> distancesGraph() const
	{
		std::vector<float> ret{};
		for (int i = 0; i < nodes.size(); i++) 
		{
			for (int j = 0; j < nodes[i]->edges.size(); j++) 
			{
				_ASSERT(nodes[i]->id == i);
				if (i == j) {
					ret.push_back(0);
				}
				ret.push_back(nodes[i]->edges[j]->dist);
			}
		}
		return ret;
	}

};