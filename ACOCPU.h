#pragma once

#include <stdio.h>
#include <string>
#include <vector>
#include <limits>
#include <algorithm>
#include <omp.h>

#include "Params.h"
#include "Timer.h"
#include "utilsCPU.h"
#include "RandGenerator.h"

namespace ACOCPU
{
	class Ant
	{
	public:
		Ant(int nodesN) : visitedNodes(std::vector<bool>(nodesN, false)), path(std::vector<int>(nodesN, -1)), propabilities(std::vector<float>(nodesN))
		{}

		Ant() = default;
		Ant(const Ant& ant) = default;
		Ant(Ant&& ant) = default;
		Ant& operator=(const Ant& ant) = default;
		Ant& operator=(Ant&& ant) = default;

		int currentNode{};
		int nextNode{};
		int totalNodesVisited{};
		float distTravelled{};

		std::vector<bool>visitedNodes; 
		std::vector<int> path;
		std::vector<float>propabilities;

		void print(int i = 0)
		{
			printf("Ant it %d, currentNode %d, nextNode %d, todalNodesVisited %d, distTravelled %f\n",
				i, currentNode, nextNode, totalNodesVisited, distTravelled
			);
		}
	};

	ACOParams params;

	std::vector<float> d_phero;
	std::vector<float> d_distances;
	std::vector<Ant> d_ants;

	std::vector<float> d_distancesHelper;

	void initializeAnts(std::vector<Ant>& d_ants, int nodesN, int antsN)
	{
		for(int j =0; j<antsN; j++)
		{
			d_ants[j] = Ant(nodesN);
			auto& ant = d_ants[j];

			ant.currentNode = RandGenerator::getRandomInRange(0, nodesN - 1);
			ant.nextNode = -1;
			ant.totalNodesVisited = 0;
			ant.path[ant.totalNodesVisited] = ant.currentNode;
			ant.visitedNodes[ant.currentNode] = true;
			ant.totalNodesVisited++;
			ant.distTravelled = 0;
		}
	}

	float calcPropability(int nodeBegin, int nodeEnd, const std::vector<float>& d_distancesHelper, 
		const std::vector<float>& d_phero, int nodesN, int alpha, int beta)
	{
		const auto& distHelper = value(d_distancesHelper.data(), nodeBegin, nodeEnd, nodesN);
#ifdef ACOA1B5
		return value(d_phero.data(), nodeBegin, nodeEnd, nodesN) * distHelper * distHelper * distHelper * distHelper * distHelper;
#else 
		return (std::powf(value(d_phero.data(), nodeBegin, nodeEnd, nodesN), alpha) * std::powf(distHelper, beta));
#endif // ACOA1B5
	}

	int NextNode(Ant& ant, const std::vector<float>& d_distancesHelper, const std::vector<float>& d_phero, int nodesNum, float alpha, float beta)
	{
		float propabilitySum{};

		if (ant.totalNodesVisited == (nodesNum - 1)) //handle last node left 
		{
			for (int i = 0; i < nodesNum; i++)
			{
				if (!ant.visitedNodes[i]) {
					return i;
				}
			}
			_ASSERT(false); // delete this; "THIS SHOULD NOT HAPPEN"
		}

		for (int i = 0; i < nodesNum; i++)
		{
			if (ant.visitedNodes[i] || i == ant.currentNode)
			{
				continue;
			}
			auto& propability = ant.propabilities[i];
			propability = calcPropability(ant.currentNode, i, d_distancesHelper, d_phero, nodesNum, alpha, beta);
			propabilitySum += propability;
		}
#ifdef _DEBUG
		if (propabilitySum == 0)
		{
			printf("propabilitySum == 0 : ");
			ant.print();
		}
#endif // _DEBUG
		
		auto random = RandGenerator::getRandomInRange(0.f, 1.f);
		float sum{};
		bool allVisited = true;

		while (true) 
		{
			for (int i = 0; i < nodesNum; i++)
			{
				if (ant.visitedNodes[i])
				{
					continue;
				}

				allVisited = false;
				sum += ant.propabilities[i] / propabilitySum;

				if (random <= sum || isnan(sum)) {
					return i;
				}
			}

			sum += 0.1;
			static int ite = 0;
			#pragma omp threadprivate(ite)
			ite++;
			random = RandGenerator::getRandomInRange(0.f, 1.f);
		
#ifdef _DEBUG
			printf("Ants propablility iters %d, random: %f, sum: %f, allVisited %d, ompNum %d \n", ite, random, sum, allVisited, omp_get_thread_num());
#endif // _DEBUG
			
			allVisited = true;
		}

		throw std::exception("none edge choosen ");
	}

	void evaporatePheromone(std::vector<float>& d_phero, const ACOParams params)
	{
		for (int i=0; i < params.nodesNum; i++)
		{
			for (int dst = 0; dst < params.nodesNum; dst++)
			{
				auto& pheroVal = value(d_phero.data(), i, dst, params.nodesNum);
				pheroVal *= (1.f - params.rho);

				_ASSERT(pheroVal >= 0);
			}
		}
	}

	void addPheromone(std::vector<float>& d_phero, std::vector<Ant>& d_ants, const ACOParams params)
	{
		for (int a=0; a < params.antsN; a++)
		{
			auto& ant = d_ants[a];
			for (auto i = 0; i + 1 < params.nodesNum; i++)
			{
				auto& src = ant.path[i];
				auto& dst = ant.path[i + 1];
				auto valToAdd = (params.q / (ant.distTravelled * ant.distTravelled));
				value(d_phero.data(), src, dst, params.nodesNum) += valToAdd;
				value(d_phero.data(), dst, src, params.nodesNum) += valToAdd;
			}
		}

	}

	void runAnts(std::vector<Ant>& d_ants, std::vector<float>& d_distances, std::vector<float>& d_phero, const ACOParams params)
	{
		for(int i=0; i<params.antsN; i++)
		{
			auto& ant = d_ants[i];

			while (ant.totalNodesVisited < params.nodesNum) //if all cites visited
			{
				ant.nextNode = NextNode(ant, d_distancesHelper, d_phero, params.nodesNum, params.alpha, params.beta);
				ant.path[ant.totalNodesVisited] = ant.nextNode;
				ant.totalNodesVisited++;

				ant.visitedNodes[ant.nextNode] = true;
				ant.distTravelled += value(d_distances.data(), ant.currentNode, ant.nextNode, params.nodesNum);
				ant.currentNode = ant.nextNode;
			}
		}
	}

	const Ant& getBestAnt(const std::vector<Ant>& vec)
	{
		return *std::min_element(vec.cbegin(), vec.cend(), [=](const Ant& a, const Ant& b) {return a.distTravelled < b.distTravelled; });
	}

	void parseResults(ACOResults& results, const std::vector<std::vector<Ant>>& antHistory)
	{
		std::vector<Ant> bestAnts{};
		for (const auto& iterAntVec : antHistory)
		{
			Ant bestAnt = getBestAnt(iterAntVec);

			bestAnts.push_back(bestAnt);

			auto iter = ACOResults::Iteration{};
			iter.bestPathLength = bestAnt.distTravelled;
			iter.bestPath = bestAnt.path;

			results.iterations.push_back(iter);
		}
	}

	void startProcessing(ACOResults& results, std::vector<std::vector<Ant>>& antHistory)
	{
		float bestDist = std::numeric_limits<float>::max();
		for (int i = 0; i < params.iterations; i++)
		{
			const std::string currentIterationName = "iteration" + std::to_string(i);
			results.timer.start(currentIterationName);

			initializeAnts(d_ants, params.nodesNum, params.antsN);
			runAnts(d_ants, d_distances, d_phero, params);
			evaporatePheromone(d_phero, params);
			addPheromone(d_phero, d_ants, params);

			results.timer.stop(currentIterationName);
			antHistory[i] = d_ants;
			auto currBestDist = getBestAnt(d_ants).distTravelled;
			bestDist = bestDist > currBestDist ? currBestDist : bestDist;
			printIteration("ACO CPU ST", results.timer.millisSinceBeginning("total"), bestDist, i, params.iterations);
		}
	}

	void startACOCPU(const ACOParams& _params, ACOResults& results)
	{
		std::cout << "ACO CPU start\n";
		params = _params;
		params.checkParams();
		
		results.timer.timerName = "ACO CPU ST";
		results.timer.start("total");

		std::vector<std::vector<Ant>> antHistory(params.iterations);

		d_distances = params.edgesDist;
		d_distancesHelper = std::vector<float>();
		std::for_each(d_distances.begin(), d_distances.end(), [](float distance) {d_distancesHelper.push_back(1.f / distance); });

		d_phero = std::vector<float>(params.edgesDist.size()+1, params.pheroInit);
		d_ants = std::vector<Ant>(params.antsN);

		startProcessing(results, antHistory);

		results.timer.stop("total");

		parseResults(results, antHistory);
	}
}

namespace ACOCPUMulti
{
	using Ant = ACOCPU::Ant;

	ACOParams params;

	std::vector<float> d_phero;
	std::vector<float> d_distances;
	std::vector<Ant> d_ants;
	std::vector<float> d_distancesHelper;


	void initializeAnts(std::vector<Ant>& d_ants, int nodesN, int antsN)
	{
		omp_set_num_threads(params.threads);
		#pragma omp parallel for
		for (int j = 0; j < antsN; j++)
		{
			d_ants[j] = Ant(nodesN);
			auto& ant = d_ants[j];

			ant.currentNode = RandGenerator::getRandomInRange(0, nodesN - 1);
			ant.nextNode = -1;
			ant.totalNodesVisited = 0;
			ant.path[ant.totalNodesVisited] = ant.currentNode;
			ant.visitedNodes[ant.currentNode] = true;
			ant.totalNodesVisited++;
			ant.distTravelled = 0;
		}
	}

	void evaporatePheromone(std::vector<float>& d_phero, const ACOParams params)
	{
		omp_set_num_threads(params.threads);
		#pragma omp parallel for
		for (int i = 0; i < params.nodesNum; i++)
		{
			for (int dst = 0; dst < params.nodesNum; dst++)
			{
				auto& pheroVal = value(d_phero.data(), i, dst, params.nodesNum);
				pheroVal *= (1.f - params.rho);

				_ASSERT(pheroVal >= 0);
			}
		}
	}

	void addPheromone(std::vector<float>& d_phero, const std::vector<Ant>& d_ants, const ACOParams params)
	{
		omp_set_num_threads(params.threads);
		#pragma omp parallel for
		for (int a = 0; a < params.antsN; a++)
		{
			auto& ant = d_ants[a];
			for (auto i = 0; i + 1 < params.nodesNum; i++)
			{
				auto& src = ant.path[i];
				auto& dst = ant.path[i + 1];
				auto valToAdd = (params.q / (ant.distTravelled * ant.distTravelled));
				value(d_phero.data(), src, dst, params.nodesNum) += valToAdd;
				value(d_phero.data(), dst, src, params.nodesNum) += valToAdd;
			}
		}
	}

	void runAnts(std::vector<Ant>& d_ants, const std::vector<float>& d_distances, const std::vector<float>& d_phero, const ACOParams& params)
	{
		omp_set_num_threads(params.threads);
		#pragma omp parallel for
		for (int i = 0; i < params.antsN; i++)
		{
			auto& ant = d_ants[i];

			while (ant.totalNodesVisited < params.nodesNum) //if all cites visited
			{
				ant.nextNode = ACOCPU::NextNode(ant, d_distancesHelper, d_phero, params.nodesNum, params.alpha, params.beta);
				ant.path[ant.totalNodesVisited] = ant.nextNode;
				ant.totalNodesVisited++;

				ant.visitedNodes[ant.nextNode] = true;
				ant.distTravelled += value(d_distances.data(), ant.currentNode, ant.nextNode, params.nodesNum);
				ant.currentNode = ant.nextNode;
			}
		}
	}

	void startProcessing(ACOResults& results, std::vector<std::vector<Ant>>& antHistory)
	{
		float bestDist = std::numeric_limits<float>::max();

		for (int i = 0; i < params.iterations; i++)
		{
			const std::string currentIterationName = "iteration" + std::to_string(i);
			results.timer.start(currentIterationName);

			ACOCPUMulti::initializeAnts(d_ants, params.nodesNum, params.antsN);
			ACOCPUMulti::runAnts(d_ants, d_distances, d_phero, params);
			ACOCPUMulti::evaporatePheromone(d_phero, params);
			ACOCPUMulti::addPheromone(d_phero, d_ants, params);

			results.timer.stop(currentIterationName);
			antHistory[i] = d_ants;

			auto currBestDist = getBestAnt(d_ants).distTravelled;
			bestDist = bestDist > currBestDist ? currBestDist : bestDist;
			printIteration("ACO CPU MT", results.timer.millisSinceBeginning("total"), bestDist, i, params.iterations);
		}
	}

	void startACOCPUMT(const ACOParams& _params, ACOResults& results)
	{
		std::cout << "ACO CPU MT start\n";
		
		params = _params;
		params.checkParams();

		results.timer.timerName = "ACO CPU MT";
		results.timer.start("total");

		std::vector<std::vector<Ant>> antHistory(params.iterations);

		d_distances = params.edgesDist;
		d_distancesHelper = std::vector<float>();
		std::for_each(d_distances.begin(), d_distances.end(), [](float distance) {d_distancesHelper.push_back(1.f/distance); });

		d_phero = std::vector<float>(params.edgesDist.size() + 1, params.pheroInit);
		d_ants = std::vector<Ant>(params.antsN);

		ACOCPUMulti::startProcessing(results, antHistory);

		results.timer.stop("total");

		parseResults(results, antHistory);
	}
}



