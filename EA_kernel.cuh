#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <limits>
#include <algorithm>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/sort.h>
#include <thrust/reduce.h>
#include <thrust/execution_policy.h>

#include "Params.h"
#include "Timer.h"
#include "utils.cuh"
#include <numeric>
#include "RandGenerator.h"

namespace eaKernel 
{
    using namespace cudaUtils;

    Individual* d_population{};
    Individual* d_population2{};
    float* d_distances{};
    double* d_percentages{};
    curandState_t* states{};

    template<bool checkDistance = true>
    __host__ void checkPopulationCorrectness(Individual* population, const int populationN, const int nodesN, std::string&& msg)
    {
#ifdef _DEBUG
        auto cpuPopulation = std::vector<Individual>(populationN);
        cudaMemcpy(cpuPopulation.data(), population, populationN * sizeof(Individual), cudaMemcpyDeviceToHost);

        for (const auto& indv : cpuPopulation) {
            std::vector<int> path{};
            path.assign(indv.geneArr, indv.geneArr + nodesN);

            if (!Results::checkAllNodesVisited(path, nodesN)) 
            {
                throw;
            };
            if constexpr (checkDistance) {
                if (indv.distance < 0 || indv.distance > nodesN * 100000)
                {
                    throw;
                }
            }
        }
        //std::cout << "population correctness ok " << msg << std::endl;
#endif // _DEBUG
    }

    template<bool countSum = false>
    __global__ void calcFitnesses(Individual* populationArr, float* sumArr, float* d_distances, const int populationN, const int nodesN) 
    {
        const int i = blockIdx.x * blockDim.x + threadIdx.x;

        if (i < populationN) 
        {
            auto& indiv = populationArr[i];
            indiv.countFitness(nodesN, d_distances);
            if constexpr (countSum) {
                sumArr[i] = indiv.distance;
            }
        }
    }

    __device__ int getElementRoulette(double* percentages, const int nodesN, const double random) 
    {
        for (int i = 0; i < nodesN; i++)
        {
            if (percentages[i] >random)
            {
                return i;
            }
#ifdef _DEBUG
            //printf("percentage[i]:%f\n", percentages[i]);
            assert(percentages[i] >= 0 && percentages[i] <= 1);
#endif // _DEBUG
        }

#ifdef _DEBUG
        printf("GetElementRoulettee random:%f\n", random);
        assert(false);
#endif // _DEBUG

        return {};
    }

    __device__ void breed(const Individual& parentA, const Individual& parentB, Individual* dstArr, const int dstIdx, const int nodesN, curandState& localState) 
    {
        const int lower = getRandomInRange_FAST(0, nodesN - 2, localState);
        const int higher = getRandomInRange_FAST(lower, nodesN - 1, localState);
        const int distance = higher - lower;

        static constexpr int nulloptVal = -1;

        int* dst = dstArr[dstIdx].geneArr;
      
        memset(dst+1, nulloptVal, (nodesN-1)*sizeof(nulloptVal));
          
        memcpy(dst + lower, parentA.geneArr + lower, distance*sizeof(int));

        bool isVisitedArr[MAX_NODES];
        memset(isVisitedArr, false, nodesN);

        for (int i = lower; i < higher; i++)
        {
            isVisitedArr[parentA.geneArr[i]] = true;
        }

        int indBIndex = 1;

        for (int i = 1; i < nodesN; i++) 
        {
            auto& retGenI = dst[i];
            if ((i >= lower && i < higher) || retGenI != nulloptVal)
                continue;

            while (isVisitedArr[parentB.geneArr[indBIndex]])
            {
                if (indBIndex + 1 < nodesN)
                    indBIndex++;
                else
                    break;
            }
            retGenI = parentB.geneArr[indBIndex];
            isVisitedArr[retGenI] = true;
        }
    }

    __global__ void fillPercentages(Individual* populationSrc, double* d_percentages, const int populationN, const float distancesSum)
    {
        const int i = blockIdx.x * blockDim.x + threadIdx.x;

        if (i == 0)
        {
            double percentageSum{};
            for (int i = 0; i < populationN; i++)
            {
                percentageSum += (double)populationSrc[i].distance / (double)distancesSum;
                d_percentages[i] = percentageSum;
            }

#ifdef _DEBUG
            //printf("percentageSum: %f\n", percentageSum);
            assert((percentageSum > 0.9999 && percentageSum < 1.00001));
#endif //DEBUG
        }
    }

    __global__ void mate(Individual* populationSrc, Individual* populationDst, double* d_percentages, const int populationN, const int nodesN, float mutationRate, int mutationTimes, curandState_t* randState)
    {
        const int i = blockIdx.x * blockDim.x + threadIdx.x;

        if (i < populationN) 
        {
            auto rand1 = getRandomInRange_0f_1f(randState[blockIdx.x * blockDim.x + threadIdx.x]);
            auto rand2 = getRandomInRange_0f_1f(randState[blockIdx.x * blockDim.x + threadIdx.x]);

            auto idx1 = getElementRoulette(d_percentages, populationN, static_cast<double>(rand1));
            auto idx2 = getElementRoulette(d_percentages, populationN, static_cast<double>(rand2));

#ifdef _DEBUG
            assert(idx1 >=0 && idx1 < populationN);
            assert(idx2 >=0 && idx2 < populationN);
#endif // _DEBUG

            breed(populationSrc[idx1], populationSrc[idx2], populationDst, i, nodesN, randState[i]);
            
            auto rand3 = getRandomInRange_0f_1f(randState[blockIdx.x * blockDim.x + threadIdx.x]);
            if (rand3 < mutationRate)
            {
                for (int j = 0; j < mutationTimes; j++) {
                    //mutateSwap(populationDst[i], nodesN, randState[blockIdx.x * blockDim.x + threadIdx.x]);
                    mutateReverse(populationDst[i], nodesN, randState[blockIdx.x * blockDim.x + threadIdx.x]);
                }
            }
        }
    }

    int eaMutationTimes(int populationN)
    {
        auto ret = populationN / 100;
        return ret > 0 ? ret : 1;
    }

    template<bool lowestDistance = true>
    __device__ int findIt(Individual* population, const int populationN) 
    {
        float memDistance = population[0].distance;
        int bestIt{};

        for (int i = 0; i < populationN; i++) 
        {
            const auto &dist = population[i].distance;
            if constexpr (lowestDistance) {

                if (dist < memDistance)
                {
                    memDistance = dist;
                    bestIt = i;
                }
            }
            else {
                if (dist > memDistance)
                {
                    memDistance = dist;
                    bestIt = i;
                }
            }
        }
        return bestIt;
    }

    __global__ void moveElite_SLOW(Individual* population, Individual* offspring, const int populationN, const int eliteSize) 
    {
        const int i = blockIdx.x * blockDim.x + threadIdx.x;
        if (i == 0) 
        {
            for (int i = 0; i < eliteSize; i++)
            {
                auto bestItPopulation = findIt(population, populationN);
                auto worstItOffspring = findIt<false>(offspring, populationN);
                memcpy(offspring +worstItOffspring, population+ bestItPopulation, sizeof(Individual));
                population[bestItPopulation].distance = 1E+37f;
            }   

            auto bestItOffspring = findIt(offspring, populationN);
        }
    }

    //returns best found distance
    double handleElite(Individual* population, Individual* offspring, const int populationN, const int eliteSize)
    {
        //Zakomentowany nie dzia�a - kompilacja nigdy sie nie konczy, brak tez powiadomienia o bledzie. 
        //thrust::device_ptr<Individual> devPopulation = thrust::device_pointer_cast(population);
        //thrust::device_ptr<Individual> devoffspring = thrust::device_pointer_cast(offspring);

        //auto vecPopulation = thrust::device_vector<Individual>(devPopulation, devPopulation + populationN);
        //auto vecOffspring = thrust::device_vector<Individual>(devoffspring, devoffspring + populationN);
        //thrust::sort(vecPopulation.begin(), vecPopulation.end(), IndvCMP());
        //thrust::sort(vecOffspring.begin(), vecOffspring.end(), IndvCMP_Inverse());

        //cudaMemcpy(offspring, population, eliteSize * sizeof(Individual), cudaMemcpyDeviceToDevice);

        auto cpuPopulation = std::vector<Individual>(populationN);
        cudaMemcpy(cpuPopulation.data(), population, populationN * sizeof(Individual), cudaMemcpyDeviceToHost);
        std::sort(cpuPopulation.begin(), cpuPopulation.end(), IndvCMP());
      
        auto cpuOffspring = std::vector<Individual>(populationN);
        cudaMemcpy(cpuOffspring.data(), offspring, populationN * sizeof(Individual), cudaMemcpyDeviceToHost);
        std::sort(cpuOffspring.begin(), cpuOffspring.end(), IndvCMP_Inverse());

        memcpy(cpuOffspring.data(), cpuPopulation.data(), eliteSize * sizeof(Individual));

        cudaMemcpy(offspring, cpuOffspring.data(), populationN * sizeof(Individual), cudaMemcpyHostToDevice);
        auto lowest = *std::min_element(cpuOffspring.begin(), cpuOffspring.end(), IndvCMP());
        return lowest.distance;
    }


    void startProcessing(const EAParams& params, EAResults& results, const CUDAParams& cudaParams) 
    {
        const size_t eliteSize = static_cast<size_t>(static_cast<float>(params.populationN) * params.elitePercentage);
        const auto cudaParamsElite = threadBlocksLUT(eliteSize);
        const auto mutationTimes = eaMutationTimes(params.populationN);

        auto distancesDVec = thrust::device_vector<float>(params.populationN);
        float* d_distancesSum = thrust::raw_pointer_cast(&distancesDVec[0]);

        auto* populationSrc = d_population;
        auto* offspring = d_population2;
        bool isPopArraysSwapped = false;
       
        double bestFound{};

        for (int i = 0; i < params.generations; i++)
        {
            const std::string currentIterationName = "iteration" + std::to_string(i);
            //results.timer.start(currentIterationName);

            calcFitnesses<true> << <cudaParams.first, cudaParams.second >> > (populationSrc, d_distancesSum, d_distances, params.populationN, params.nodesNum);
            checkCudaError();

            checkPopulationCorrectness(populationSrc, params.populationN, params.nodesNum, "first popSRC");

            auto popDistancesSum = thrust::reduce(distancesDVec.begin(), distancesDVec.end(), 0.f);

            fillPercentages << <1, 1 >> > (populationSrc, d_percentages, params.populationN, popDistancesSum);
            checkCudaError();

            mate <<< cudaParams.first, cudaParams.second >>> (populationSrc, offspring, d_percentages, params.populationN, 
                params.nodesNum,  params.mutationRate, mutationTimes, states);
            checkCudaError();

            checkPopulationCorrectness(populationSrc, params.populationN, params.nodesNum, "second after mate popSRC");
            checkPopulationCorrectness<false>(offspring, params.populationN, params.nodesNum, "second after mate offspring");

            calcFitnesses<false> << <cudaParams.first, cudaParams.second >> > (offspring, nullptr, d_distances, params.populationN, params.nodesNum);
            checkCudaError();

            checkPopulationCorrectness(populationSrc, params.populationN, params.nodesNum, "3 popSRC");
            checkPopulationCorrectness(offspring, params.populationN, params.nodesNum, "3 offspring");

            results.timer.start("handleElite"+currentIterationName);
            
            bestFound = handleElite(populationSrc, offspring, params.populationN, eliteSize);

            //moveElite_SLOW << <1, 1 >> > (populationSrc, offspring, params.populationN, eliteSize);
            results.timer.stop("handleElite" + currentIterationName);
            checkCudaError(); 

            checkPopulationCorrectness<false>(populationSrc, params.populationN, params.nodesNum, "4 last popSRC");
            checkPopulationCorrectness(offspring, params.populationN, params.nodesNum, "4 last offspring");

            std::swap(populationSrc, offspring);
            isPopArraysSwapped = !isPopArraysSwapped;

            printIterationCUDA("EA CUDA", results.timer.millisSinceBeginning("total"), bestFound, i, params.generations);

       }

        std::vector<Individual>finalPopulation(params.populationN);
        auto* devicePopulationPtr = isPopArraysSwapped ? populationSrc : offspring;
        cudaMemcpy(finalPopulation.data(), devicePopulationPtr, sizeof(Individual) * params.populationN, cudaMemcpyDeviceToHost);

        auto lowestElementIt = std::min_element(finalPopulation.begin(), finalPopulation.end(), [](const auto& el1, const auto& el2) {return el1.distance < el2.distance; });
        _ASSERT(lowestElementIt != finalPopulation.end());
        auto lowestElement = *lowestElementIt;
        results.iterations.emplace_back(Results::Iteration(std::vector<int>(lowestElement.geneArr, lowestElement.geneArr + params.nodesNum), lowestElement.distance));
    }

    void cudaEAalgorithm(const EAParams& params, EAResults& results)
    {
        results.timer.start("total");
        const auto edgesDist = params.edgesDist;

        const auto cudaParams = threadBlocksLUT(params.populationN);

        RAII_MALLOC<decltype(states)> randStatesRAII(states, cudaParams.first * cudaParams.second* sizeof(curandState_t));

        initRandomStates << <cudaParams.first, cudaParams.second >> > (states, time(0));
        checkCudaError();

        auto initPopulation = createInitPopulation(params.populationN, params.nodesNum);

        RAII_MALLOC<decltype(d_distances)> distancesRAII(d_distances, edgesDist.size() * sizeof(float));
        RAII_MALLOC<decltype(d_population)> populationRAII(d_population, initPopulation.size() * sizeof(Individual));
        RAII_MALLOC<decltype(d_population2)> population2RAII(d_population2, initPopulation.size() * sizeof(Individual));
        RAII_MALLOC<decltype(d_percentages)> percentagesRAII(d_percentages, params.nodesNum * sizeof(double));

        cudaMemcpy(d_distances, edgesDist.data(), edgesDist.size() * sizeof(float), cudaMemcpyHostToDevice);
        checkCudaError();

        cudaMemcpy(d_population, initPopulation.data(), initPopulation.size() * sizeof(Individual), cudaMemcpyHostToDevice);
        checkCudaError();

        startProcessing(params, results, cudaParams);
        results.timer.stop("total");
    }
}