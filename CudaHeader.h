#pragma once
#include "Params.h"

namespace cudaTest {
	void invokeCudaFunc();
}
namespace acoKernel {
	void cudaAntAlgorithm(const ACOParams& params, ACOResults& results);
}

namespace eaKernel {
	void cudaEAalgorithm(const EAParams& _params, EAResults& results);
}

namespace saKernel 
{
	void cudaSaAlgorithm(const SAParams& _params, SAResults& results);
}

