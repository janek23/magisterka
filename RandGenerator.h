#pragma once
#include <random>

namespace RandGenerator 
{
    static std::random_device rd;
    static std::mt19937 mt(rd());

    inline int getRandomInRange(int min, int max)
    {
        std::uniform_int_distribution<int> distr(min, max);
        return distr(mt);
    }

    inline float getRandomInRange(float min, float max) {
        std::uniform_real_distribution<float> distr(min, max);
        return distr(mt);
    }

    inline double getRandomInRange(double min, double max)
    {
        std::uniform_real_distribution<double> distr(min, max);
        return distr(mt);
    }
}

//inline float getRandom0to1() {
//    float divBase = 1000000;
//    std::uniform_int_distribution<int> distr(0, divBase);
//    return static_cast<float>(distr(mt)) / divBase;
//}