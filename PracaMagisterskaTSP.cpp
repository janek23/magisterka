#include <iostream>
#include "CudaHeader.h"
#include "Graph.h"
#include "FileManager.h"
#include <thread>
#include <chrono>
#include "View.h"
#include "ViewModel.h"

std::thread viewThread;

int main()
{
    auto view = View();
    auto viewModel = std::make_shared<ViewModel>();
    view.viewModel = viewModel;

    viewThread = std::thread(&View::startView, &view);
    viewThread.detach();

    while (!viewThread.joinable()) 
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(500ms);
    }
    viewThread.join();
    return 0;
}
