#pragma once

#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <limits>
#include <algorithm>
#include <thrust/device_vector.h>

#include "Params.h"
#include "Timer.h"
#include "utils.cuh"

namespace acoKernel 
{
	using namespace cudaUtils;

	class Ant
	{
	public:
		int currentNode{};
		int nextNode{};
		int totalNodesVisited{};
		float distTravelled{};

		bool visitedNodes[MAX_NODES];
		int path[MAX_NODES];
		float propabilities[MAX_NODES];

		__device__ void print(int i = 0)
		{
			printf("Ant it %d, currentNode %d, nextNode %d, todalNodesVisited %d, distTravelled %f\n",
				i, currentNode, nextNode, totalNodesVisited, distTravelled
			);
		}
	};

	ACOParams params;
	CUDAParams cudaParams;

	float* d_phero{};
	float* d_distances{};
	float* d_distancesHelper{};
	Ant* d_ants{};
	curandState_t* states{};

	__device__ void initializeAnt(Ant& ant, curandState& localState, int nodesN)
	{
		for (int i = 0; i < nodesN; i++)
		{
			ant.visitedNodes[i] = false;
			ant.path[i] = -1;
		}

		ant.currentNode = getRandomInRange_FAST(0, nodesN - 1, localState);
		ant.nextNode = -1;
		ant.totalNodesVisited = 0;
		ant.path[ant.totalNodesVisited] = ant.currentNode;
		ant.visitedNodes[ant.currentNode] = true;
		ant.totalNodesVisited++;
		ant.distTravelled = 0;
	}

	__device__ float calcPropability(int nodeBegin, int nodeEnd, float* d_distances, float* d_distancesHelper, float* d_phero, int nodesN, int alpha, int beta)
	{
		const auto& distHelper = value(d_distancesHelper, nodeBegin, nodeEnd, nodesN);
#ifdef ACOA1B5
		return value(d_phero, nodeBegin, nodeEnd, nodesN) * distHelper * distHelper * distHelper * distHelper * distHelper;
#else 
		return (powf(value(d_phero, nodeBegin, nodeEnd, nodesN), alpha) * powf(distHelper, beta));
#endif // ACOA1B5
	}

	__device__ int NextNode(Ant& ant, float* d_distances, float* d_distancesHelper, float* d_phero, curandState_t* randState, int nodesNum, float alpha, float beta)
	{
		float propabilitySum{};

		if (ant.totalNodesVisited == (nodesNum - 1)) //handle last node left 
		{
			for (int i = 0; i < nodesNum; i++)
			{
				if (!ant.visitedNodes[i]) {
					return i;
				}
			}
		}

		for (int i = 0; i < nodesNum; i++)
		{
			if (ant.visitedNodes[i] || i == ant.currentNode)
			{
				continue;
			}
			auto& propability = ant.propabilities[i];
			propability = calcPropability(ant.currentNode, i, d_distances, d_distancesHelper, d_phero, nodesNum, alpha, beta);
			propabilitySum += propability;
		}
		if (propabilitySum == 0)
		{
			printf("propabilitySum == 0 : ");
			ant.print();
		}

		auto random = getRandomInRange_0f_1f(randState[blockIdx.x * blockDim.x + threadIdx.x]);
		float sum{};
		bool allVisited = true;

		while (true) {
			for (int i = 0; i < nodesNum; i++)
			{
				if (ant.visitedNodes[i])
				{
					continue;
				}

				allVisited = false;
				sum += ant.propabilities[i] / propabilitySum;

				if (random <= sum || isnan(sum)) {
					return i;
				}
			}

			sum += 0.1;
			static int ite = 0;
			ite++;
			random = getRandomInRange_0f_1f(randState[blockIdx.x * blockDim.x + threadIdx.x]);

			printf("Ants propablility iters %d, random: %f, sum: %f, allVisited %d\n", ite, random, sum, allVisited);

			allVisited = true;
		}

		assert(false);//none edge choosen 
	}

	__global__ void evaporatePheromone(float* d_phero, const ACOParams params)
	{
		const int src = blockIdx.x * blockDim.x + threadIdx.x;

		if (src < params.nodesNum)
		{
			for (int dst = 0; dst < params.nodesNum; dst++)
			{
				auto& pheroVal = value(d_phero, src, dst, params.nodesNum);
				pheroVal *= (1.f - params.rho);

				assert(pheroVal >= 0);
			}
		}
	}

	__global__ void addPheromone(float* d_phero, Ant* d_ants, const ACOParams params)
	{
		const int a = blockIdx.x * blockDim.x + threadIdx.x;

		if (a < params.antsN)
		{
			auto& ant = d_ants[a];
			for (auto i = 0; i + 1 < params.nodesNum; i++)
			{
				auto& src = ant.path[i];
				auto& dst = ant.path[i + 1];
				auto valToAdd = (params.q / (ant.distTravelled * ant.distTravelled));
				value(d_phero, src, dst, params.nodesNum) += valToAdd;
				value(d_phero, dst, src, params.nodesNum) += valToAdd;
			}
		}

	}

	__global__ void runAnts(Ant* d_ants, float* d_distances, float* d_distancesHelper, float* d_phero, curandState_t* randState, const ACOParams params)
	{
		const int i = blockIdx.x * blockDim.x + threadIdx.x;

		if (i < params.antsN)
		{
			auto& ant = d_ants[i];
			
			initializeAnt(ant, randState[i], params.nodesNum);

			while (ant.totalNodesVisited < params.nodesNum) //if all cites visited
			{
				ant.nextNode = NextNode(ant, d_distances, d_distancesHelper, d_phero, randState, params.nodesNum, params.alpha, params.beta);
				ant.path[ant.totalNodesVisited] = ant.nextNode;
				ant.totalNodesVisited++;

				ant.visitedNodes[ant.nextNode] = true;
				ant.distTravelled += value(d_distances, ant.currentNode, ant.nextNode, params.nodesNum);
				ant.currentNode = ant.nextNode;
			}
		}
	}

	const Ant& getBestAnt(const std::vector<Ant>& vec)
	{
		return *std::min_element(vec.cbegin(), vec.cend(), [=](const Ant& a, const Ant& b) {return a.distTravelled < b.distTravelled; });
	}

	void parseResults(ACOResults& results, const std::vector<std::vector<Ant>>& antHistory)
	{
		for (const auto& iterAntVec : antHistory)
		{
			auto bestAnt = getBestAnt(iterAntVec);

			auto iter = ACOResults::Iteration{};
			iter.bestPathLength = bestAnt.distTravelled;
			iter.bestPath.resize(params.nodesNum);
			for (int i = 0; i < params.nodesNum; i++)
			{
				iter.bestPath[i] = (bestAnt.path[i]);
			}

			results.iterations.push_back(iter);
		}
	}

	void startProcessing(ACOResults& results, std::vector<std::vector<Ant>>& antHistory, const CUDAParams& antCudaParams )
	{
		const auto edgesCudaParams = threadBlocksLUT(params.edgesNum);

		float bestDist = std::numeric_limits<float>::max();

		for (int i = 0; i < params.iterations; i++)
		{
			const std::string currentIterationName = "iteration" + std::to_string(i);
			results.timer.start(currentIterationName);

			runAnts << <antCudaParams.first, antCudaParams.second >> > (d_ants, d_distances, d_distancesHelper, d_phero, states, params);

			checkCudaError();
			cudaDeviceSynchronize();

			evaporatePheromone << <edgesCudaParams.first, edgesCudaParams.second >> > (d_phero, params);

			checkCudaError();
			cudaDeviceSynchronize();

			addPheromone << <antCudaParams.first, antCudaParams.second >> > (d_phero, d_ants, params);

			checkCudaError();

			std::vector<Ant> ants(params.antsN);
			cudaMemcpy(ants.data(), d_ants, sizeof(Ant) * params.antsN, cudaMemcpyDeviceToHost);
			antHistory[i] = ants;

			checkCudaError();

			cudaMemset(d_ants, 0, params.antsN * sizeof(Ant)); //reset ants

			results.timer.stop(currentIterationName);

			auto currBestDist = getBestAnt(ants).distTravelled;
			bestDist = bestDist > currBestDist ? currBestDist : bestDist;
			printIterationCUDA("ACO CUDA", results.timer.millisSinceBeginning("total"), bestDist, i, params.iterations);
			//printf("ACO CUDA iteration %d/%d  itTime: %f [ms]\n",i, params.iterations, results.timer.get(currentIterationName).getMillis());
		}
	}

	void cudaAntAlgorithm(const ACOParams& _params, ACOResults& results)
	{
		params = _params;
		params.checkParams();
		
		const auto antCudaParams = threadBlocksLUT(params.antsN);
		_ASSERT(_params.nodesNum <= MAX_NODES);

		cudaDeviceReset();

		results.timer.timerName = "ACO CUDA";
		results.timer.start("total");
		
		std::vector<std::vector<Ant>> antHistory(params.iterations);
		auto& edgesDist = params.edgesDist;
		const auto cudaParams = threadBlocksLUT(params.antsN);
		//here we define how many random states will be generated - each cuda core should have own state
		RAII_MALLOC<decltype(states)> randStatesRAII(states, cudaParams.first * cudaParams.second * sizeof(curandState_t));

		initRandomStates << <cudaParams.first, cudaParams.second>> > (states, time(0));
		checkCudaError();

		std::vector<float> pheromones(params.edgesDist.size());
		std::fill(pheromones.begin(), pheromones.end(), params.pheroInit);
		RAII_MALLOC<decltype(d_phero)> pheroRAII(d_phero, pheromones.size() * sizeof(float));

		RAII_MALLOC<decltype(d_distances)> distancesRAII(d_distances, edgesDist.size() * sizeof(float));
		RAII_MALLOC<decltype(d_distancesHelper)> distancesHelperRAII(d_distancesHelper, edgesDist.size() * sizeof(float));
		RAII_MALLOC<decltype(d_ants)> antsRAII(d_ants, params.antsN * sizeof(Ant));

		std::vector<float> distancesHelperVec = std::vector<float>();
		std::for_each(edgesDist.begin(), edgesDist.end(), [&](float distance) {distancesHelperVec.push_back(1.f / distance); });

		cudaMemcpy(d_distancesHelper, distancesHelperVec.data(), distancesHelperVec.size() * sizeof(float), cudaMemcpyHostToDevice);
		checkCudaError();

		cudaMemcpy(d_distances, edgesDist.data(), edgesDist.size() * sizeof(float), cudaMemcpyHostToDevice);
		checkCudaError();

		cudaMemcpy(d_phero, pheromones.data(), pheromones.size() * sizeof(float), cudaMemcpyHostToDevice);
		checkCudaError();

		startProcessing(results, antHistory, cudaParams);

		results.timer.stop("total");

		parseResults(results, antHistory);
	}
}



