#pragma once

#include "Params.h"
#include "RandGenerator.h"
#include <algorithm>
#include <mutex>
#include <thread>

namespace SACPU
{
    using Temp = SAParams::Temp;

    SAParams params;

    int acceptedWorsed{};
    int itWoImproval{};

    void updateTempLog(Temp& temp, int n) {
        temp = params.Tmax / std::log(n + 1);
    }

    void updateTempLinear(Temp& temp, double multipler) 
    {
        temp *= multipler;
    }

    double acceptanceFunct(auto delta, Temp temp)
    {
        return std::exp(-delta / temp);
    }

    bool isSolutionAccepted(auto distBefore, auto distAfter, Temp temp)
    {
        if (distAfter < distBefore)
            return true;

        auto random = RandGenerator::getRandomInRange(0., 1.);
        if ( acceptanceFunct(distAfter - distBefore, temp) > random)
        {
            acceptedWorsed++;
            return true;
        }

        return false;
    }

    void startSACPU(const SAParams& _params, SAResults& results)
    {
        std::cout << "SA CPU ST start\n";

        params = _params;
        params.checkParams();

        acceptedWorsed = {};
        itWoImproval = {};

        results.timer.timerName = "SA CPU ST";
        results.timer.start("total");

        Temp temp = params.Tmax;

        auto indv = Individual(params.nodesNum);
        indv.fillRandom();
        //indv.distance = std::numeric_limits<float>::max();
        indv.countFitness(params.edgesDist.data());

        float lowestDist = std::numeric_limits<float>::max();
        std::vector<int> bestSolutionVec(params.nodesNum);

        auto bestIndv = Individual(params.nodesNum);
        double random{};

        int n{};
        int nToCompare = {};
        while (temp > params.Tmin && n < params.maxIterations && itWoImproval < params.iterStopIfStuck)
        {
            auto distBefore = indv.distance;
            auto reverseParams = indv.reverseSA();

            indv.countFitnessSAReverse(params.edgesDist.data(), reverseParams);
            const auto& distAfter = indv.distance;

            if (isSolutionAccepted(distBefore, distAfter, temp))
            {
                lowestDist = distAfter;
                bestSolutionVec = indv.gene;
                itWoImproval = {};
                updateTempLinear(temp, 1 - 0.000001);
                //updateTempLog(temp, n);
            }
            else
            {
                indv.reverse(reverseParams);
                indv.distance = distBefore;
                itWoImproval++;
            }
        
            if (n== nToCompare) {
                nToCompare += params.maxIterations/100;
                printIteration("SA CPU ST", results.timer.millisSinceBeginning("total"), lowestDist, n, params.maxIterations);
            }
            n++;
        }

        std::cout << "FINAL iter:" << n << " temp:" << temp << " bestLenghth:" << lowestDist <<" accepted worsed:"<<acceptedWorsed 
            << " it without improval:"<<itWoImproval<< std::endl;

        Results::Iteration iter{};
        iter.bestPath = bestSolutionVec;
        iter.bestPathLength = lowestDist;
        results.iterations.emplace_back(iter);

        results.timer.stop("total");
    }
    
}

namespace SACPUMulti
{
    using Temp = SAParams::Temp;
    SAParams params;

    struct ThreadData 
    {
        ThreadData(const SAParams& _params) : bestIndv{ Individual(_params.nodesNum) }, indv{ Individual(_params.nodesNum) }, temp{ _params.Tmax }, params(_params), processing(true)
        {
            indv.fillRandom();
            indv.distance = std::numeric_limits<float>::max();
        };

        static inline std::atomic_bool runThreads = true;
        std::atomic<bool> processing = true;
       
        SAParams params;

        uint64_t iterations{};
        int itWoImproval = {};
        int acceptedWorsed = {};

        Temp temp = params.Tmax;
        int n{};
        
        Individual bestIndv;
        Individual indv;
    };
 
    void loop(ThreadData& data) 
    {
        while (true) 
        {
            if (data.processing)
            {
                data.n = 0;
                data.indv.countFitness(data.params.edgesDist.data());

                while (data.temp > data.params.Tmin && data.n < data.params.iterWoSync)
                {
                    data.n++;
                    auto distBefore = data.indv.distance;
                    auto reverseParams = data.indv.reverseSA();

                    data.indv.countFitnessSAReverse(data.params.edgesDist.data(), reverseParams);
                    const auto& distAfter = data.indv.distance;

                    if (SACPU::isSolutionAccepted(distBefore, distAfter, data.temp))
                    {
                        data.bestIndv = data.indv;
                        SACPU::updateTempLinear(data.temp, 1 - 0.000001);
                    }
                    else
                    {
                        data.indv.reverse(reverseParams);
                        data.indv.distance = distBefore;
                    }
                }
                data.processing = false;
            }
            if (!data.runThreads) 
            {
                return;
            }
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(5ms);
        }
    }

    void startSACPUMulti(const SAParams& _params, SAResults& results)
    {
        std::cout << "SA CPU MT start\n";

        params = _params;
        params.checkParams();

        results.timer.timerName = "SA CPU MT";
        results.timer.start("total");

        ThreadData::runThreads = true;
        const int numThreads = _params.threads;

        std::atomic<uint64_t> totalIterations{};

        auto threadData = std::vector<std::unique_ptr<ThreadData>>();
        for (int n = 0; n < numThreads; n++)
        {
            threadData.emplace_back(new ThreadData(params));
        }

        std::vector<std::thread> threads(numThreads);

        Individual bestGlobalIndv(params.nodesNum);
        bestGlobalIndv.distance = std::numeric_limits<decltype(Individual::distance)>::max();
        Temp bestGlobalIndvTemp;

        for (int n = 0; n < numThreads; n++)
        {
            threads[n] = std::thread(SACPUMulti::loop, std::ref(*threadData[n].get()));
        }

        bool runAlgo = true;
        int it{};
        const int counterStart = (_params.maxIterations / _params.iterWoSync)/10;
        int counter = counterStart;

        while (runAlgo)
        {
            for (auto& tDataPtr : threadData)
            {
                auto& tData = *tDataPtr;
                if (!tData.processing)
                {
                    it++;
                    totalIterations += tData.n;

                    if (tData.bestIndv.distance < bestGlobalIndv.distance)
                    {
                        bestGlobalIndv = tData.bestIndv;
                        bestGlobalIndvTemp = tData.temp;
                    }
                    else if (tData.bestIndv.distance > bestGlobalIndv.distance)
                    {
                        tData.indv = bestGlobalIndv;
                        tData.temp = bestGlobalIndvTemp;
                    }

                    if (totalIterations >= _params.maxIterations)
                    {
                        runAlgo = false;
                        tData.runThreads = false;
                        break;
                    }
                    if (it == counter) {
                        counter += counterStart;
                        printIteration("SA CPU MT", results.timer.millisSinceBeginning("total"), bestGlobalIndv.distance, totalIterations, params.maxIterations);
                    }
                }
                tData.processing = true;
            }
        }
        
        for (auto& thread : threads) 
        {
            thread.join();
        }

        results.timer.stop("total");

        Results::Iteration iter{};
        iter.bestPath = bestGlobalIndv.gene;
        iter.bestPathLength = bestGlobalIndv.distance;
        std::cout << "total Iterations:" << totalIterations << std::endl;

        results.iterations.emplace_back(iter);
    }



}