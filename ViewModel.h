#pragma once
#include <memory>
#include <mutex>

#include "Graph.h"
#include "FileManager.h"
#include "CudaHeader.h"
#include "Params.h"
#include "EACPU.h"
#include "SACPU.h"
#include "ACOCPU.h"

class ViewModel
{
public:
    std::vector<sf::VertexArray> edges{};
    std::vector<sf::CircleShape> nodes{};

    Graph graph;

    ACOParams acoParams{};
    EAParams eaParams{};
    SAParams saParams{};

    CPUParams cpuParams{};

    const std::string graphsPath = "graphs";
    static inline std::atomic_bool isTestRun = false;
    struct RandomGraphParams
    {
        int nElements = 10000;
        float xMin = 0.f;
        float xMax = 1000.f;
        float yMin = 0.f;
        float yMax = 1000.f;
    }randomGrapParams;

    void setRandomGraph()
    {
        graph = Graph::generateRandomGraph(randomGrapParams.xMin, randomGrapParams.xMax,
            randomGrapParams.yMin, randomGrapParams.yMax, randomGrapParams.nElements);
    }

    void generateSFMLgraph() {
        setNodes(graph.nodes);
        //setEdges(graph.edges);
    }

    void generateSFMLgraphFromPath(const std::vector<int>& path) 
    {
        setNodes(graph.nodes);
        auto edges = graph.getPathEdgesVec(path);
        setEdges(edges);
    }

    void setNodes(const std::vector<std::shared_ptr<Graph::Node>>& graphNodes)
    {
        std::scoped_lock sl(_mtxNodes, _mtxTexts);
        nodes.clear();
        //texts.clear();
        for (const auto& node : graphNodes) {
            _ASSERT(node.get());
            nodes.emplace_back(std::move(getNodeDrawing(*node.get())));
            //texts.push_back(getNodeTextDrawing(*node.get()));
        }
    }

    void setEdges(const std::vector<std::shared_ptr<Graph::Edge>>& graphEdges)
    {
        std::scoped_lock sl(_mtxEdges);
        edges.clear();
        for (const auto& edge : graphEdges) {
            _ASSERT(edge.get());
            edges.emplace_back(std::move(getEdgeDrawing(*edge.get())));
        }
    }

    sf::VertexArray getEdgeDrawing(const Graph::Edge& edge, const sf::Color& color = sf::Color::Red)
    {
        sf::VertexArray line(sf::LineStrip, 2);
        line[0].position = { static_cast<float>(edge.a->x) , static_cast<float>(edge.a->y) };
        line[1].position = { static_cast<float>(edge.b->x) , static_cast<float>(edge.b->y) };

        line[0].color = color;
        line[1].color = color;

        return line;
    }

    static sf::CircleShape getNodeDrawing(const Graph::Node& node, const sf::Color& color = sf::Color::Green)
    {
        sf::CircleShape shape(4);
        shape.setPosition(node.x, node.y);
        shape.setFillColor(color);
        return shape;
    }

    bool saveGraph(const std::string fileName)
    {
        if (fileName == "" || !tspPrecheck())
            return false;

        auto json = FileManager::serializeGraph(graph);
        return FileManager::saveFile(json.dump(), fileName, graphsPath);
    }

    void loadGraphFromFile(const std::string fileName) 
    {
        if (fileName == "")
            return;

        auto file = FileManager::loadFromFile(fileName, graphsPath);
        graph = FileManager::deserializeGraph(file);
    }

    auto getFilenames() {
        return FileManager::getFileNames(graphsPath);
    }

    bool tspPrecheck() 
    {
        if (graph.nodes.size() < 3) {
            std::cerr << "Graph not selected \n";
            return false;
        }

        return true; 
    }

    void parseViewResults(Results& results) 
    {
#ifdef _DEBUG
        results.checkErrors();
#endif // _DEBUG

        std::cout << "RUN SUMMARY" << std::endl;
        std::cout << results.params->algoName << std::endl;
        
        float bestLength = std::min_element(results.iterations.begin(), results.iterations.end(), [](const auto& a, const auto& b) { return a.bestPathLength < b.bestPathLength;})->bestPathLength;
        std::cout << "Best Length: " << bestLength << std::endl;
        
        //std::cout << results.timer.toString() << std::endl;

        //for (int i = 0; i < results.iterations.size(); i++)
        //{
        //    std::cout << "it: " << i << " dist: " << results.iterations[i].bestPathLength << "\n";
        //}
        std::cout << "END SUMMARY" << std::endl;

        if (!isTestRun) {
            auto bestIteration = results.getBestIteration();
            generateSFMLgraphFromPath(bestIteration.bestPath);
        }
    }

    void runCudaKernel() 
    {
        cudaTest::invokeCudaFunc();
    }

private:
    void checkPrintParams(std::string name, auto& params)
    {
        std::cout << name<<" ALGO INVOKED\n";
        std::cout << params.toString();
        params.algoName = name;
        params.checkParams();
    }
public: 

    void runACOKernel() 
    {
        if (!tspPrecheck())
            return;

        acoParams.edgesDist = graph.distancesGraph();
        acoParams.edgesNum = graph.edges.size();
        acoParams.nodesNum = graph.nodes.size();

        checkPrintParams("CUDA ACO", acoParams);

        ACOResults results{};
        results.params = &acoParams;

        acoKernel::cudaAntAlgorithm(acoParams, results);

        parseViewResults(results);
    }

    void runACOCPU() 
    {
        if (!tspPrecheck())
            return;

        acoParams.edgesDist = graph.distancesGraph();
        acoParams.edgesNum = graph.edges.size();
        acoParams.nodesNum = graph.nodes.size();
        acoParams.threads = cpuParams.threads;
        acoParams.iterWoSync = cpuParams.iterWoSync;

        checkPrintParams("CPU ACO", acoParams);

        ACOResults results{};
        results.params = &acoParams;

        ACOCPU::startACOCPU(acoParams, results);

        parseViewResults(results);
    }

    void runACOCPUMT()
    {
        if (!tspPrecheck())
            return;

        acoParams.edgesDist = graph.distancesGraph();
        acoParams.edgesNum = graph.edges.size();
        acoParams.nodesNum = graph.nodes.size();
        acoParams.threads = cpuParams.threads;
        acoParams.iterWoSync = cpuParams.iterWoSync;

        checkPrintParams("CPU MT ACO", acoParams);

        ACOResults results{};
        results.params = &acoParams;

        ACOCPUMulti::startACOCPUMT(acoParams, results);

        parseViewResults(results);
    }

    void runEACPU() 
    {
        if (!tspPrecheck())
            return;

        eaParams.edgesDist = graph.distancesGraph();
        eaParams.edgesNum = graph.edges.size();
        eaParams.nodesNum = graph.nodes.size();

        checkPrintParams("CPU EA", eaParams);
    
        EAResults results;
        results.params = &eaParams;

        eaCPU::startEACPU(eaParams, results);

        parseViewResults(results);
    }

    void runEACPUMulti() 
    {
        if (!tspPrecheck())
            return;

        eaParams.edgesDist = graph.distancesGraph();
        eaParams.edgesNum = graph.edges.size();
        eaParams.nodesNum = graph.nodes.size(); 
        eaParams.threads = cpuParams.threads;
        eaParams.iterWoSync = cpuParams.iterWoSync;

        if (!(eaParams.populationN >= eaParams.threads) or !(eaParams.populationN % eaParams.threads == 0))
        {
            std::cerr << "ERROR populationN %cpuThreads != 0 \n";
            return;
        }

        checkPrintParams("CPU MT EA", eaParams);

        EAResults results;
        results.params = &eaParams;

        EACPUMulti::startEACPUMulti(eaParams, results);

        parseViewResults(results);
    }

    void runEAGPU() 
    {
        if (!tspPrecheck())
            return;

        eaParams.edgesDist = graph.distancesGraph();
        eaParams.edgesNum = graph.edges.size();
        eaParams.nodesNum = graph.nodes.size();

        checkPrintParams("CUDA EA", eaParams);

        EAResults results;
        results.params = &eaParams;

        eaKernel::cudaEAalgorithm(eaParams, results);
    
        parseViewResults(results);
    }

    void runSACPU() 
    {
        if (!tspPrecheck())
            return;

        saParams.edgesDist = graph.distancesGraph();
        saParams.edgesNum = graph.edges.size();
        saParams.nodesNum = graph.nodes.size();

        checkPrintParams("CPU SA", saParams);
        
        SAResults results; 
        results.params = &saParams;

        if (saParams.iterStopIfStuck == 0)
            saParams.iterStopIfStuck = std::numeric_limits<decltype(saParams.iterStopIfStuck)>::max();

        SACPU::startSACPU(saParams, results);

        parseViewResults(results);
    }

    void runSACPUMulti() 
    {
        if (!tspPrecheck())
            return;

        saParams.edgesDist = graph.distancesGraph();
        saParams.edgesNum = graph.edges.size();
        saParams.nodesNum = graph.nodes.size();
        saParams.threads = cpuParams.threads;
        saParams.iterWoSync = cpuParams.iterWoSync;

        checkPrintParams("CPU MT SA", saParams);

        SAResults results;
        results.params = &saParams;

        SACPUMulti::startSACPUMulti(saParams, results);

        parseViewResults(results);
    }

    void runSAGPU() 
    {
        if (!tspPrecheck())
            return;

        saParams.edgesDist = graph.distancesGraph();
        saParams.edgesNum = graph.edges.size();
        saParams.nodesNum = graph.nodes.size();
        saParams.threads = cpuParams.threads;
        saParams.iterWoSync = cpuParams.iterWoSync;

        checkPrintParams("CUDA SA", saParams);

        SAResults results;
        results.params = &saParams;
    
        saKernel::cudaSaAlgorithm(saParams, results);

        parseViewResults(results);
    }

    void runPerformanceTests() 
    {
        isTestRun = true;
        //acoPerformanceTests();
        //eaPerformanceTests();
        //saPerformanceTests();

        tspTimeTests();

        isTestRun = false;
    }

    void acoPerformanceTests() 
    {
        std::vector<int> sizes
        {
            5000
            //50, 
            //100, 
            //200, 
            //400,
            //600, 
        };
        auto getStringNameFromInf = [](int n) {return std::to_string(n) + ".json"; };

        acoParams.iterations = 5;
        //acoParams.rho = 0.3;
        int ite{};
        for (const auto& size : sizes)
        {
            ite++;
            loadGraphFromFile(getStringNameFromInf(size));

            //runACOCPU();
            //runACOKernel();
            runACOCPUMT();

            std::cout << std::endl << "TESTS FINISHED ITERATION!!" << ite << "/" << sizes.size() << "\n\n";
        }
    }

    void eaPerformanceTests() 
    {
        std::vector<int> sizes
        {
            50, 
            100, 
            200, 
            400,
            600 
        };
        auto getStringNameFromInf = [](int n) {return std::to_string(n) + ".json"; };

        int ite{};
        for (const auto& size : sizes)
        {
            ite++;
            loadGraphFromFile(getStringNameFromInf(size));

            //runEACPU();
            //runEACPUMulti();
            runEAGPU();

            std::cout << std::endl << "TESTS FINISHED ITERATION!!" << ite << "/" << sizes.size() << "\n\n";
        }

    }

    void saPerformanceTests()
    {
        std::vector<int> sizes
        {
            50,
            100,
            200,
            400,
            600,
            5000
        };
        auto getStringNameFromInf = [](int n) {return std::to_string(n) + ".json"; };

        int ite{};
        for (const auto& size : sizes)
        {
            ite++;
            loadGraphFromFile(getStringNameFromInf(size));

            runSACPU();
            runSACPUMulti();
            runSAGPU();

            std::cout << std::endl << "TESTS FINISHED ITERATION!!" << ite << "/" << sizes.size() << "\n\n";
        }

    }

    void tspTimeTests() 
    {
        std::vector<int> sizes
        {
            //50,
            //100,
            //200,
            400,
            //600
        };
        auto getStringNameFromInf = [](int n) {return std::to_string(n) + ".json"; };

        for (const auto& size : sizes)
        {
            loadGraphFromFile(getStringNameFromInf(size));
            acoParams.iterations = 50;
            runACOCPU();
            runACOCPUMT();
            acoParams.iterations = 100;
            runACOKernel();

            cpuParams.iterWoSync = 100;
            eaParams.generations = 5000;

            runEACPU();
            cpuParams.iterWoSync = 100;
            eaParams.generations = 300000;
            runEACPUMulti();

            eaParams.generations = 500;
            runEAGPU();

            cpuParams.iterWoSync = 10000;

            saParams.maxIterations = 80000000;
            runSACPU();
            runSACPUMulti();
            saParams.maxIterations = 2000000;
            runSAGPU();

            std::cout << std::endl << "Tests for size: " << size << " finished" << std::endl;
        }
        std::cout << std::endl << "All tests finished" << std::endl;
    }

    std::mutex _mtxNodes, _mtxTexts, _mtxEdges;
};

