#pragma once

template<typename T>
static const T& value(const T const * data, const int node1, const int node2, const int nodesN)
{
    return *(data + (node1 * nodesN + node2));
}

template<typename T>
static T& value(T* data, const int node1, const int node2, const int nodesN)
{
   return *(data + (node1 * nodesN + node2));
}

void printIteration(const std::string& algoName, double totalTime, float bestResult, int currentIt, int maxIt)
{
    currentIt++;
    std::cout << std::fixed << std::setprecision(3)<< algoName << " time: " << totalTime << " [ms] result: " << bestResult << " "<<currentIt<<"/"<<maxIt<<" "<< currentIt * 100 / maxIt << "[%]" << std::endl;
}

struct Individual
{
    constexpr static inline int nulloptVal = -1;

    std::vector<int> gene;
    float distance = (float)nulloptVal;

    Individual(size_t nodesN) :gene(std::vector<int>(nodesN, nulloptVal))
    {
        _ASSERT(nodesN > 2);
        gene[0] = 0;
    }

    void fillRandom()
    {
        _ASSERT(gene.size() > 3);
        gene[0] = 0;
        std::iota(gene.begin() + 1, gene.end(), 1);
        std::shuffle(gene.begin() + 1, gene.end(), RandGenerator::mt);
    }

    void countFitness(float const* edgesDistPtr)
    {
        distance = 0;
        const int nodesN = gene.size();
        for (int i = 0; i + 1 < nodesN; i++)
        {
            distance += value(edgesDistPtr, gene[i], gene[i + 1], nodesN);
        }
        distance += value(edgesDistPtr, gene[gene.size() - 1], gene[0], nodesN);
    }

    void countFitnessSAReverse(float const* edgesDistPtr, const std::pair<int, int>& reverseParams)
    {
        const auto& [lower, higher] = reverseParams;

        if (lower == higher)
            return;
        
        const int nodesN = gene.size();

        auto A = lower !=0 ? lower - 1 : nodesN -1;
        auto B = higher - 1;

        auto& AB = value(edgesDistPtr, gene[A], gene[B], nodesN);
        auto& higherLower = value(edgesDistPtr, gene[higher], gene[lower], nodesN);
        auto& Alower = value(edgesDistPtr, gene[A], gene[lower], nodesN);
        auto& Bhigher = value(edgesDistPtr, gene[B], gene[higher], nodesN);

        distance = distance - AB - higherLower + Alower + Bhigher;
    }

    void mutate()
    {
        int lower = RandGenerator::getRandomInRange(1, gene.size() - 2);
        int higher = RandGenerator::getRandomInRange(lower, gene.size() - 1);
        std::swap(gene[lower], gene[higher]);
    }

    void mutate(const std::pair<int, int>& mutateParams)
    {
        const auto& lower = mutateParams.first;
        const auto& higher = mutateParams.second;
        _ASSERT(lower < gene.size() && higher < gene.size() && lower >= 0 && higher >= 0);

        std::swap(gene[lower], gene[higher]);
    }

    const std::pair<int, int> mutateSA()
    {
        int lower = RandGenerator::getRandomInRange(1, gene.size() - 2);
        int higher = RandGenerator::getRandomInRange(lower, gene.size() - 1);
        mutate({ lower, higher });
        return { lower, higher };
    }

    void reverse(const std::pair<int, int>& reverseParam) 
    {
        const auto& [lower, higher] = reverseParam;
        _ASSERT(lower < gene.size() && higher < gene.size() && lower >= 0 && higher >= 0);
        std::reverse(gene.begin() + lower, gene.begin() + higher);
    }

    std::pair<int, int> reverseSA()
    {
        int lower = RandGenerator::getRandomInRange(0, gene.size() - 2);
        int higher = RandGenerator::getRandomInRange(lower, gene.size() - 1);
        const std::pair<int, int> ret = std::make_pair(lower, higher);
        reverse(ret);
        return ret;
    }
};
