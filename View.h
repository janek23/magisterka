#pragma once

#include <TGUI/TGUI.hpp>
#include <memory>
#include <thread>
#include <chrono>
#include <future>

#include "Graph.h"
#include "ViewModel.h"

class View
{
public:
    constexpr static unsigned int fps = 30;
    constexpr static float frameTime = 1000.f / fps;

    std::shared_ptr<ViewModel> viewModel;

    //make this private
    constexpr static inline auto titleLabelTextSize = 30;
    constexpr static inline auto stdLabelTextSize = 15;
    constexpr static inline auto spaceSize = 0.3;
    constexpr static inline auto xAxisIntendation = 25;
    constexpr static inline auto xAxisIntendation2 = 50;

    constexpr static inline auto xAxisSecondColumn = 550; 
    constexpr static inline auto xSecondColumnIntendation = xAxisSecondColumn + xAxisIntendation;
    constexpr static inline auto xSecondColumnIntendation2 = xAxisSecondColumn + xAxisIntendation2;
    constexpr static inline auto xButtonMargin = 20;

    tgui::Label::Ptr addLabelToLayout(const std::string& text, int textSize, auto& layout, const tgui::Layout2d& position, const std::string& name = {})
    {
        auto label = tgui::Label::create();
        label->setText(text);
        label->setTextSize(textSize);
        label->setAutoSize(true);
        label->setPosition(position);
        if (name.empty()) {
            layout->add(label);
        }
        else {
            layout->add(label, name);
        }

        return label;
    }

    tgui::Button::Ptr addButtonToLayout(const std::string& text, auto function, auto& layout, const tgui::Layout2d& position)
    {
        auto button = tgui::Button::create(text);
        button->onClick(function);
        button->setPosition(position);
        layout->add(button);
        return button;
    }

    tgui::EditBox::Ptr addEditBoxToLayout(const std::string& text, auto* param, auto& layout, const tgui::Layout2d& position)
    {
        auto editBox = tgui::EditBox::create();
        editBox->setDefaultText(text);
        editBox->setPosition(position);

        std::stringstream ss;
        ss.precision(2);
        ss << *param;
        editBox->setText(ss.str());

        using T = std::remove_pointer_t<decltype(param)>;

        if constexpr (std::is_same<std::string, T>::value)
        {
            editBox->onTextChange([=]() {*param = editBox->getText().toStdString(); });
        }
        else if constexpr (std::is_same<int, T>::value) {
            editBox->onTextChange([=]() 
                {
                    try { *param = std::stoi(editBox->getText().toStdString()); } 
                    catch (...){};
        });
        }
        else if constexpr (std::is_same<float, T>::value)
        {
            editBox->onTextChange([=]() {
                try { *param = std::stof(editBox->getText().toStdString()); }
                catch (...) {};
                });
        }
        else if constexpr (std::is_same<double, T>::value)
        {
            editBox->onTextChange([=]() {
                try { *param = std::stod(editBox->getText().toStdString()); }
                catch (...) {};
                });
        }
        else
            static_assert(false, "modifiedParam is unknown param");

        layout->add(editBox);
        return editBox;
    }
    
    void createUILayout(std::unique_ptr<tgui::GuiSFML>& windowTGUI)
    {
        int yPos = 0;

        //GRAPH
        addLabelToLayout("Graph", titleLabelTextSize, windowTGUI, { 0,yPos });
        addLabelToLayout("Generate new graph", stdLabelTextSize, windowTGUI, { xAxisIntendation, yPos += 35 });
        auto labelGraphSize = addLabelToLayout(graphSizeLabelText + ": " + std::to_string(viewModel->randomGrapParams.nElements), stdLabelTextSize, windowTGUI, { xAxisIntendation2, yPos += 25 }, "labelGraphSize");

        auto sliderGraphSize = tgui::Slider::create();
        sliderGraphSize->setMinimum(3);
        sliderGraphSize->setMaximum(400);
        sliderGraphSize->setValue(viewModel->randomGrapParams.nElements);
        sliderGraphSize->setStep(1.f);
        sliderGraphSize->onValueChange([&]() 
            {
                viewModel->randomGrapParams.nElements = static_cast<int>((windowTGUI->get<tgui::Slider>("sliderGraphSize"))->getValue()); 
                setNodesCountSizeText(windowTGUI->get<tgui::Label>("labelGraphSize"));
            }
        );
        sliderGraphSize->setPosition(xAxisIntendation2, yPos += 25);
        sliderGraphSize->setWidth(400);
        windowTGUI->add(sliderGraphSize, "sliderGraphSize");

        addButtonToLayout("Generate", [this]() {generateNewGraph(); }, windowTGUI, { xAxisIntendation2, yPos += 30 });

        addLabelToLayout("Load from file", stdLabelTextSize, windowTGUI, { xAxisIntendation, yPos += 35 });

        auto filenamesListBox = tgui::ListBox::create();
        setFilesListBox(filenamesListBox);
        filenamesListBox->onItemSelect([this, filenamesListBox]() {selectedGraphFilenameToLoad = filenamesListBox->getSelectedItem().toStdString(); });
        filenamesListBox->setPosition({ xAxisIntendation2, yPos += 30 });
        windowTGUI->add(filenamesListBox);

        addButtonToLayout("Load selected", [this]() {loadGraphFromFile(); }, windowTGUI, { xAxisIntendation2, yPos += 150 });

        addLabelToLayout("Save to file", stdLabelTextSize, windowTGUI, { xAxisIntendation, yPos += 35 });

        auto editBoxFileName = addEditBoxToLayout("insert file name", &graphFilenameInput, windowTGUI, { xAxisIntendation2, yPos += 30 });
        editBoxFileName->setWidth(400);

        addButtonToLayout("Save graph to file", [this]() {saveGraph(); }, windowTGUI, { xAxisIntendation2, yPos += 30 });

        //VIEW 
        addLabelToLayout("View", titleLabelTextSize, windowTGUI, { 0,yPos += 30 });
        addButtonToLayout("Enable graph view", [this]() {ifShowGraphWindow ? hideGraphWindow() : showGraphWindow(); }, windowTGUI, { xAxisIntendation, yPos += 35 });

        //SECOND COLUMN 
        auto yPosCol = 0;
        addLabelToLayout("Algorithms", titleLabelTextSize, windowTGUI, { xAxisSecondColumn, yPosCol });

        auto columnVariable = xSecondColumnIntendation;

        auto gpuAcoButton = addButtonToLayout("GPU ACO", [this]() {viewRunACOKernel(); }, windowTGUI, { xSecondColumnIntendation, yPosCol += 40 });
        auto cpuAcoButton = addButtonToLayout("CPU ACO", [this]() {viewRunACOCPU(); }, windowTGUI, { columnVariable += gpuAcoButton.get()->getSize().x + xButtonMargin / 2, yPosCol });
        auto cpuAcoMTButton = addButtonToLayout("CPU ACO MT", [this]() {viewRunACOCPUMT(); }, windowTGUI, { columnVariable += cpuAcoButton.get()->getSize().x + xButtonMargin / 2, yPosCol });

        auto gpuEaButton = addButtonToLayout("GPU EA", [this]() {viewRunEaGPU(); }, windowTGUI, { columnVariable += cpuAcoMTButton.get()->getSize().x + xButtonMargin*4 , yPosCol });
        auto cpuEaButton = addButtonToLayout("CPU EA", [this]() {viewRunEaCPU(); }, windowTGUI, { columnVariable += gpuEaButton.get()->getSize().x + xButtonMargin/2, yPosCol});
        auto cpuEaMultiButton = addButtonToLayout("CPU EA MT", [this]() {viewRunEaCPUMulti(); }, windowTGUI, { columnVariable += gpuEaButton.get()->getSize().x + xButtonMargin / 2, yPosCol });
        
        //second row
        columnVariable = xSecondColumnIntendation;
        auto gpuSaButton = addButtonToLayout("GPU SA", [this]() {viewRunSAGPU(); }, windowTGUI, { columnVariable, yPosCol += 30 });
        auto cpuSaButton = addButtonToLayout("CPU SA", [this]() {viewRunSACPU(); }, windowTGUI, { columnVariable += gpuSaButton.get()->getSize().x + xButtonMargin / 2  , yPosCol });
        auto cpuMultiSaButton = addButtonToLayout("CPU SA MT", [this]() {viewRunSACPUMutli(); }, windowTGUI, { columnVariable += cpuSaButton.get()->getSize().x + xButtonMargin /2 , yPosCol });
        
        auto gpuDbgButton = addButtonToLayout("CHECK CUDA", [this]() {viewRunCudaKernel(); }, windowTGUI, { columnVariable += gpuSaButton.get()->getSize().x + xButtonMargin*4 , yPosCol });
        auto perfTestsButton = addButtonToLayout("PERF TESTS", [this]() {viewRunPerformanceTests(); }, windowTGUI, { columnVariable += gpuDbgButton.get()->getSize().x + xButtonMargin * 4 , yPosCol });

        addLabelToLayout("Params", titleLabelTextSize, windowTGUI, { xAxisSecondColumn, yPosCol += 30 });

        addLabelToLayout("Hardware settings", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation, yPosCol += 35 });
        addLabelToLayout("CPU Threads", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 35 });
        addEditBoxToLayout("CPU threads", &viewModel->cpuParams.threads, windowTGUI, { xSecondColumnIntendation2 + 100, yPosCol });

        addLabelToLayout("Iter wo sync", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2+300, yPosCol });
        addEditBoxToLayout("Iter wo sync", &viewModel->cpuParams.iterWoSync, windowTGUI, { xSecondColumnIntendation2 + 300+100, yPosCol });

        addLabelToLayout("ACO", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation, yPosCol += 35 });

        const auto yPosAcoParams2Col = yPosCol;
        auto yPosAco2ColModi = yPosAcoParams2Col;
        const auto xCol2EditBox = 100;

        addLabelToLayout("iterations", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 35 });
        addLabelToLayout("ants num", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });
        addLabelToLayout("phe init", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });
        addLabelToLayout("alpha", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });

        addEditBoxToLayout("iter", &viewModel->acoParams.iterations, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosAco2ColModi += 35 });
        addEditBoxToLayout("antsN", &viewModel->acoParams.antsN, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosAco2ColModi += 30 });
        addEditBoxToLayout("pherInit", &viewModel->acoParams.pheroInit, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosAco2ColModi += 30 });
        addEditBoxToLayout("phero importance", &viewModel->acoParams.alpha, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosAco2ColModi += 30 });

        yPosAco2ColModi = yPosAcoParams2Col;
        const auto xCol2Labels2Row = 300;

        addLabelToLayout("beta", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2 + xCol2Labels2Row, yPosAco2ColModi += 35 });
        addLabelToLayout("rho", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2 + xCol2Labels2Row, yPosAco2ColModi += 30 });
        addLabelToLayout("q", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2 + xCol2Labels2Row, yPosAco2ColModi += 30 });

        yPosAco2ColModi = yPosAcoParams2Col;

        addEditBoxToLayout("node importance", &viewModel->acoParams.beta, windowTGUI, { xSecondColumnIntendation2 + xCol2Labels2Row + xCol2EditBox, yPosAco2ColModi += 35 });
        addEditBoxToLayout("phero evaporation", &viewModel->acoParams.rho, windowTGUI, { xSecondColumnIntendation2 + xCol2Labels2Row + xCol2EditBox, yPosAco2ColModi += 30 });
        addEditBoxToLayout("phero left by ant", &viewModel->acoParams.q, windowTGUI, { xSecondColumnIntendation2 + xCol2Labels2Row + xCol2EditBox, yPosAco2ColModi += 30 });
   
        addLabelToLayout("EA", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation, yPosCol += 35 });

        const auto yPosGaParams2Col = yPosCol;
        auto yPosGa2ColModi = yPosGaParams2Col;

        addLabelToLayout("population", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 35 });
        addLabelToLayout("generations", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });
        addLabelToLayout("elite %", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });
        addLabelToLayout("mutations %", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });

        addEditBoxToLayout("population", &viewModel->eaParams.populationN, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 35 });
        addEditBoxToLayout("generations", &viewModel->eaParams.generations, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 30 });
        addEditBoxToLayout("elite", &viewModel->eaParams.elitePercentage, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 30 });
        addEditBoxToLayout("mutations", &viewModel->eaParams.mutationRate, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 30 });


        addLabelToLayout("SA", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation, yPosCol += 35 });
        yPosGa2ColModi = yPosCol;

        addLabelToLayout("iterations", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 35 });
        addLabelToLayout("stop if stuck", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });
        addLabelToLayout("T begin", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });
        addLabelToLayout("T end", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });
        addLabelToLayout("CUDA threads", stdLabelTextSize, windowTGUI, { xSecondColumnIntendation2, yPosCol += 30 });

        addEditBoxToLayout("iterations", &viewModel->saParams.maxIterations, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 35 });
        addEditBoxToLayout("stopIfStuck", &viewModel->saParams.iterStopIfStuck, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 30 });
        addEditBoxToLayout("T begin", &viewModel->saParams.Tmax, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 30 });
        addEditBoxToLayout("T end", &viewModel->saParams.Tmin, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 30 });
        addEditBoxToLayout("CUDA threads", &viewModel->saParams.CUDAThreads, windowTGUI, { xSecondColumnIntendation2 + xCol2EditBox, yPosGa2ColModi += 30 });
    }

    void startView()
    {
        window = std::unique_ptr<sf::RenderWindow>(new sf::RenderWindow({ 1280, 768 }, "TGUI window with SFML"));
        window->setFramerateLimit(fps);
        window->setVerticalSyncEnabled(true);

        windowTGUI = std::unique_ptr<tgui::GuiSFML>(new tgui::GuiSFML(*window.get()));
        
        createUILayout(windowTGUI);
        while (window->isOpen())
        {
            if (!ifShowWindow) {
                hideGraphWindow();
                window->close();
                return;
            }

            sf::Event event;
            
            while (window->pollEvent(event))
            {
                windowTGUI->handleEvent(event);
                if (event.type == sf::Event::Closed) {
                    window->close();
                    return;
                }           
                else if (event.type == sf::Event::Resized)
                {
                   //handle graph resize here but in another window
                }
            }
            window->clear(sf::Color(255, 255, 255));
            windowTGUI->draw();
            window->display();

            //graphWindow
            if (ifShowGraphWindow and graphWindow)
            {
                while (graphWindow->pollEvent(event))
                {
                    if (event.type == sf::Event::Closed) {
                        hideGraphWindow();
                    }
                }
                graphWindow->clear(sf::Color(255, 255, 255));
                {
                    std::scoped_lock sl(viewModel->_mtxEdges);
                    for (const auto& edge : viewModel->edges) {
                        graphWindow->draw(edge);
                    }
                }
                {
                    std::scoped_lock sl(viewModel->_mtxNodes);
                    for (const auto& node : viewModel->nodes) {
                        graphWindow->draw(node);
                    }
                }
                graphWindow->display();
            }
            
            std::this_thread::sleep_for(std::chrono::milliseconds((int)frameTime));
        }
    }

private:
    std::unique_ptr<sf::RenderWindow> graphWindow = nullptr;
    std::unique_ptr<sf::RenderWindow> window = nullptr;
    std::unique_ptr<tgui::GuiSFML> windowTGUI = nullptr;

    std::atomic_bool ifShowWindow = true;
    std::atomic_bool ifShowGraphWindow = false;

    std::string graphFilenameInput = "";
    std::string selectedGraphFilenameToLoad = "";
    const std::string graphSizeLabelText = "size";

    void showGraphWindow() 
    {
        ifShowGraphWindow = true;
        graphWindow = std::unique_ptr<sf::RenderWindow>(new sf::RenderWindow({1000, 1000}, "Graph"));
        graphWindow->setFramerateLimit(fps);
        updateGraphWindow();
    }

    void hideGraphWindow() 
    {
        ifShowGraphWindow = false;
        if (graphWindow) {
            graphWindow->close();
        }
        //graphWindow.release();
    }

    void updateGraphWindow() 
    {
        if (ifShowGraphWindow and graphWindow)
        {
            viewModel->generateSFMLgraph();
        }
    }

    void setNodesCountSizeText(tgui::Label::Ptr label)
    {
        if (viewModel) {
            label->setText(graphSizeLabelText + ": " + std::to_string(viewModel->randomGrapParams.nElements));
        }
    }

    void setRandomGraph() 
    {
        const auto& widgets = windowTGUI->getWidgets();
        viewModel->setRandomGraph();
    }

    void generateNewGraph() {
        viewModel->setRandomGraph();
        updateGraphWindow();
        std::cout << "Graph generated: " << viewModel->randomGrapParams.nElements << std::endl;
    }

    void saveGraph() 
    {
       bool status = viewModel->saveGraph(graphFilenameInput);
       if (status)
       {
           std::cout << "Saved graph: " << graphFilenameInput << std::endl;
       }
       else 
       {
           std::cout << "FAILED to Saved graph: " << graphFilenameInput << std::endl;
       }
    }

    void loadGraphFromFile() 
    {
        viewModel->loadGraphFromFile(selectedGraphFilenameToLoad);
        std::cout << "Loaded graph: " << selectedGraphFilenameToLoad << std::endl;
    }

    auto getGraphsFileNames() {
        return viewModel->getFilenames();
    }

    void setFilesListBox(tgui::ListBox::Ptr lb) 
    {
        for (const auto filename : getGraphsFileNames())
            lb->addItem(filename);
    }

    void viewRunCudaKernel() 
    {
       viewModel->runCudaKernel();
    }

    void viewRunACOKernel() 
    {
        std::thread([this]() {viewModel->runACOKernel(); }).detach();
    }

    void viewRunACOCPU() 
    {
        std::thread([this]() {viewModel->runACOCPU(); }).detach();
    }

    void viewRunACOCPUMT()
    {
        std::thread([this]() {viewModel->runACOCPUMT(); }).detach();
    }

    void viewRunEaCPU()
    {
        std::thread([this]() {viewModel->runEACPU(); }).detach();
    }

    void viewRunEaCPUMulti() 
    {
        std::thread([this]() {viewModel->runEACPUMulti(); }).detach();
    }

    void viewRunEaGPU() 
    {
        std::thread([this]() {viewModel->runEAGPU(); }).detach();
    }

    void viewRunSACPU(){
        std::thread([this]() { viewModel->runSACPU(); }).detach();
    }

    void viewRunSACPUMutli()
    {
        std::thread([this]() {viewModel->runSACPUMulti(); }).detach();
    }

    void viewRunSAGPU() 
    {
        std::thread([this]() {viewModel->runSAGPU(); }).detach();
    }

    void viewRunPerformanceTests() 
    {
        std::thread([this]() {viewModel->runPerformanceTests(); }).detach();
    }

};

