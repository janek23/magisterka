#pragma once
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <string>
#include <vector>

#include "utils.cuh"

namespace cudaTest {
	using namespace cudaUtils;

	constexpr unsigned int numBlocks = 1024;
	constexpr unsigned int blockSize = 512;

	template<typename T>
	__global__ void cudaFunc(T* a, T* b, T* c, size_t size, curandState* state)
	{
		unsigned int index = blockIdx.x * blockDim.x + threadIdx.x;
		unsigned int stride = blockDim.x * gridDim.x;

		for (int i = index; i < size; i += stride) {
			a[i] = getRandomInRange_FAST(0, 1000, state[index]);
			b[i] = getRandomInRange_FAST(0, 1000, state[index]);
			c[i] = a[i] + b[i];
		}
	}

	curandState_t* randStates{};
	void invokeCudaFunc()
	{
		cudaDeviceReset();
		checkCudaDevices();

		const size_t size = int(1e7);

		printf("variable states is at address A: %p\n", (void*)&randStates);
		RAII_MALLOC<decltype(randStates)>asdasd(randStates, blockSize * numBlocks * sizeof(curandState_t));

		initRandomStates << <numBlocks, blockSize >> > (randStates, time(0));
		checkCudaError();

		std::vector<unsigned int> h_a(size);
		std::vector<unsigned int> h_b(size);
		std::vector<unsigned int> h_c(size);

		unsigned int* d_a, * d_b, * d_c;

		cudaMalloc(&d_a, h_a.size() * sizeof(unsigned int));
		cudaMalloc(&d_b, size * sizeof(unsigned int));
		cudaMalloc(&d_c, size * sizeof(unsigned int));

		cudaFunc << <numBlocks, blockSize >> > (d_a, d_b, d_c, size, randStates);
		cudaDeviceSynchronize();
		cudaMemcpy(h_a.data(), d_a, h_a.size() * sizeof(unsigned int), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_b.data(), d_b, size * sizeof(unsigned int), cudaMemcpyDeviceToHost);
		cudaMemcpy(h_c.data(), d_c, size * sizeof(unsigned int), cudaMemcpyDeviceToHost);


		auto ret = cudaFree(d_a);
		printf("CUDA FREE a STATUS: %d\n", (int)ret);
		ret = cudaFree(d_b);
		printf("CUDA FREE b STATUS: %d\n", (int)ret);
		ret = cudaFree(d_c);
		printf("CUDA FREE c STATUS: %d\n", (int)ret);
	}
}