#pragma once

#include <vector>
#include <sstream>
#include <set>

#include "Timer.h"

//constexpr size_t MAX_NODES = 5000;//hack set also in utils.cuh

struct CPUParams
{
	int threads = 12;
	int iterWoSync = 1000;

	std::string toString() const {
		std::stringstream ss;
		ss << "threads: " << threads << "\n";
		ss << "iterWoSync: " << iterWoSync << "\n";
		return ss.str();
	}
};

struct Params : public CPUParams 
{
	std::vector<float> edgesDist;
	size_t edgesNum;
	size_t nodesNum;

	std::string algoName{};

	void checkParams()
	{
		_ASSERT(edgesDist.size() > 2);
		_ASSERT(edgesNum > 2);
		_ASSERT(nodesNum > 2);
		_ASSERT(threads > 0 && threads < 1000);
	}

	std::string toString() const  
	{
	return "nodesN: " + std::to_string(nodesNum)+ "\n";
	}
};

struct ACOParams :public Params
{
	float pheroInit = 50;
	int iterations = 30;
	int antsN = 768;

	float alpha = 1; //phero importance
	float beta = 5;  //node importance
	float rho = 0.5; //phero evaportaion
	float q = 100; //phero left by ant

	void checkParams() 
	{
		Params::checkParams();

		_ASSERT(pheroInit >= 0);
		_ASSERT(iterations > 0);
		_ASSERT(antsN > 0);
		_ASSERT(alpha > 0);
		_ASSERT(beta > 0);
		_ASSERT(rho > 0);
		_ASSERT(q > 0);

#ifdef  ACOA1B5
		_ASSERT(alpha == 1 && beta == 5);
#endif //  ACOA1B5
	}
	
	std::string toString() const 
	{
		std::stringstream ss;
		ss <<static_cast<const Params&>(*this).toString()
			<< static_cast<const CPUParams&>(*this).toString()
			<< "pheroInit: " << pheroInit << "\n"
			<< "iterations: " << iterations << "\n"
			<< "antsN: " << antsN << "\n"
			<< "alpha :" << alpha << "\n"
			<< "beta:" << beta << "\n"
			<< "rho:" << rho << "\n"
			<< "q:" << q << "\n";
		return ss.str();
	}
};

struct EAParams :public Params
{
	int populationN = 768;
	int generations = 500; 
	float elitePercentage = 0.2f;
	float mutationRate = 0.01f;

	void checkParams() {
		Params::checkParams();
		_ASSERT(populationN > 0);
		_ASSERT(generations > 0);
		_ASSERT(elitePercentage >= 0 && elitePercentage< 1);
		_ASSERT(mutationRate >= 0 && mutationRate <= 1);
	}

	std::string toString() const
	{
		std::stringstream ss;
		ss << static_cast<const Params&>(*this).toString()
			<< static_cast<const CPUParams&>(*this).toString()
			<< "populationN: " << populationN << "\n"
			<< "generations: " << generations << "\n"
			<< "elitePercentage: " << elitePercentage << "\n"
			<< "mutationRate :" << mutationRate << "\n";
		return ss.str();
	}
};

struct SAParams :public Params
{
	using Temp = double;
	int maxIterations = 2000000;
	int iterStopIfStuck = maxIterations / 20;
	Temp Tmax = 1;
	Temp Tmin = 0.05;
	int CUDAThreads = 768;

	std::string toString() const
	{
		std::stringstream ss;
		ss << static_cast<const Params&>(*this).toString()
			<< static_cast<const CPUParams&>(*this).toString()
			<< "maxIterations: " << maxIterations << "\n"
			<< "iterStopIfStuck: " << iterStopIfStuck << "\n"
			<< "Tmax: " << Tmax << "\n"
			<< "Tmin :" << Tmin << "\n";
		return ss.str();
	}
};

struct Results
{
	struct Iteration 
	{
		std::vector<int> bestPath;
		float bestPathLength;

		Iteration() {}

		Iteration(std::vector<int> _bestPath, float _bestPathLength) : bestPathLength(_bestPathLength) {
			bestPath = std::vector<int>(_bestPath.begin(), _bestPath.end());
		}

		Iteration(const Iteration& iter) {
			bestPathLength = iter.bestPathLength;
			bestPath = std::vector<int>(iter.bestPath);
		}
	};
	Params* params;

	std::vector<Iteration> iterations;

	Timer timer;

	std::string toString() const 
	{
		std::stringstream ss;
		ss << std::setw(2);
		ss << "Best lengths:" << std::endl;
		//for (int i =0; i<iterations.size(); i++) 
		//{
		//	ss << i<< "bestLength" << " " << iterations[i].bestPathLength<<std::endl;
		//	ss << printPath(iterations[i].bestPath) << "\n";
		//}
		ss << timer.toString();
		return ss.str();
	}

	bool checkErrors() const 
	{
		for (const auto& iter : iterations) 
		{
			if (!checkAllNodesVisited(iter.bestPath, params->nodesNum))
			{
				std::cerr <<"!!!!!!!!!!!!!!INCORECT SOLUTION:\n" +printPath(iter.bestPath)+"\n";
				throw std::exception("Incorrect solution. Algo error!\n");
				return false;
			}
		}
		std::cout << "OK - NO ERRORS\n";
		return true;
	}

	static bool checkAllNodesVisited(const std::vector<int>& path, const int nodesN)
	{
		for (const auto& node : path) {
			if (node < 0 || node > nodesN)
				return false;
		}

		std::set<int> set{ path.cbegin(), path.cend() };

		if (set.size() != nodesN) {
			int nodeBefore = -1;
			for (const auto& node : set) {
				if (node != nodeBefore + 1) {
					std::cout << node << "<---" << std::endl;
				}
				else {
					std::cout << node << std::endl;
				}
				nodeBefore = node;
			}
		}

		return set.size() == nodesN;
	}

	static std::string printPath(const std::vector<int>& path) 
	{
		std::stringstream ss;
		ss << 0 << "| ";
		for (int i = 0; i < path.size(); i++) 
		{
			ss << path[i] << " ";
			if (i % 10 == 0 && i!=0) {
				ss << std::endl;
				ss << i << "| ";
			}
		}
		ss << std::endl;
		return ss.str();

	}

	const Iteration& getBestIteration() const
	{
		_ASSERT(iterations.size() >= 1);
		return *std::min_element(iterations.begin(), iterations.end(), [](const auto& it1, const auto& it2) {return it1.bestPathLength < it2.bestPathLength; });
	}
};

struct ACOResults : public Results {};

struct EAResults : public Results {};

struct SAResults : public Results {};

using CUDAParams = std::pair<int, int>;

